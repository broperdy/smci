﻿using Core.Configuration;

namespace SMCi.WebCore.Extensions
{
    public static class ConfigurationExtensions
    {
        public static string UploadFolder(this ISettingManager setting)
        {
            return setting.GetNotEmptyValue("UploadFolder").TrimEnd('/');
        }
    }
}
