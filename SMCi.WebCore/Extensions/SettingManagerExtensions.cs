﻿using System;
using System.Linq;
using Core.Configuration;

namespace SMCi.WebCore.Extensions
{
    public static class SettingManagerExtensions
    {
        public static string WebDomain(this ISettingManager manager)
        {
            return manager.GetNotEmptyValue("Web.Domain");
        }

        public static string RegisterUrl(this ISettingManager manager)
        {
            return string.Format("{0}{1}", manager.WebDomain(), manager.GetNotEmptyValue("Web.RegisterUrl"));
        }

        public static string ConfirmEmailUrl(this ISettingManager manager)
        {
            return string.Format("{0}{1}", manager.WebDomain(), manager.GetNotEmptyValue("Web.ConfirmEmailUrl"));
        }

        public static string ResetPasswordUrl(this ISettingManager manager)
        {
            return string.Format("{0}{1}", manager.WebDomain(), manager.GetNotEmptyValue("Web.ResetPasswordUrl"));
        }

        public static string RegisterResellerUrl(this ISettingManager manager)
        {
            return string.Format("{0}{1}", manager.WebDomain(), manager.GetNotEmptyValue("Web.RegisterResellerUrl"));
        }

        public static double[] MapLocation(this ISettingManager settings)
        {
            try
            {
                var location = settings.GetValue("MapLocation").Split(',')
                              .Select(x => Convert.ToDouble(x.Trim()))
                              .ToList();
                return new[] { location[0], location[1] };
            }
            catch
            {
                return new[] { .0, .0 };
            }
        }

        public static string AdminEmail(this ISettingManager manager)
        {
            return manager.GetNotEmptyValue("Email.Admin");
        }

        public  static string DefaultLang(this ISettingManager manager)
        {
            return manager.GetNotEmptyValue("DefaultLanguage");
        }
    }
}