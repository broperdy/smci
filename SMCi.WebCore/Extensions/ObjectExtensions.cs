﻿using System;

namespace SMCi.WebCore.Extensions
{
    public static class ObjectExtensions
    {
        public static string ToDateTimeString(this object date)
        {
            try
            {
                var dateString = date.ToString();
                return dateString.Substring(6, 2) + "/" + dateString.Substring(4, 2) + "/" + dateString.Substring(0, 4);
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
