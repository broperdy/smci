﻿using System.Diagnostics;
using CacheManager.Core;
using SimpleInjector;

namespace SMCi.WebCore.Extensions
{
    public static class ContainerExtensions
    {
        [DebuggerStepThrough]

        public static void RegisterCacheManager(this Container container)
        {
            var cacheConfig = ConfigurationBuilder.BuildConfiguration(settings =>
            {
                settings.WithSystemRuntimeCacheHandle("inprocess");
            });

            container.RegisterSingleton(() => CacheFactory.FromConfiguration<object>(cacheConfig));

            container.ResolveUnregisteredType += (sender, e) =>
            {
                if (e.UnregisteredServiceType.IsGenericType &&
                    e.UnregisteredServiceType.GetGenericTypeDefinition() == typeof(ICacheManager<>))
                {
                    e.Register(() => CacheFactory.FromConfiguration(
                        e.UnregisteredServiceType.GetGenericArguments()[0],
                        cacheConfig));
                }
            };
        }
    }
}
