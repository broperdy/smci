using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Net.Mail.Template;

namespace SMCi.WebCore.Extensions
{
    public static class TemplateEmailSenderExtensions
    {
        public static Task SendForgotPassword(this ITemplateEmailSender service, string email, string requestUrl)
        {
            var dict = new Dictionary<string, string>
            {
                ["Href"] = requestUrl,
            };
            return service.SendEmailAsync("ForgotPassword", email, "Reset your password", dict);
        }

        public static Task SendConfirmEmail(this ITemplateEmailSender service, string email, string requestUrl)
        {
            var dict = new Dictionary<string, string>
            {
                ["Href"] = requestUrl,
            };
            return service.SendEmailAsync("ConfirmEmail", email, "Confirm your email", dict);
        }

        public static Task SendRegisterEmail(this ITemplateEmailSender service, string email, string requestUrl)
        {
            var dict = new Dictionary<string, string>
            {
                ["Href"] = requestUrl,
            };
            return service.SendEmailAsync("RegisterEmail", email, "Register your account", dict);
        }
        public static Task SendUserResetPassword(this ITemplateEmailSender service, string email, string password)
        {
            var dict = new Dictionary<string, string>
            {
                ["Password"] = password
            };
            return service.SendEmailAsync("ResetPassword", email, "Password reset", dict);
        }
    }
}