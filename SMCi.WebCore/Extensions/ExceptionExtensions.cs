﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SMCi.WebCore.Extensions
{
    public static class ExceptionExtensions
    {
        private static readonly IDictionary<string, string> Messages;

        private static class ViolationNumbers
        {
            public const int UniqueKey = 2627;
            public const int ForeignKey = 547;
        }

        static ExceptionExtensions()
        {
            Messages = new Dictionary<string, string>
            {
                ["IX_Slug"] = "Cannot insert/update duplicate slug"
            };
        }

        public static bool HandleSqlException(this Exception ex, ref string message)
        {
            var sqlException = ex.GetBaseException() as SqlException;
            if (sqlException == null) return false;

            switch (sqlException.Number)
            {
                case ViolationNumbers.UniqueKey:
                    message = GetUserFriendlyMessage(sqlException.Message);
                    return true;
                case ViolationNumbers.ForeignKey:
                    message = "The record cannot be deleted because it is associated with another record(s)";
                    return true;
            }
            return false;
        }

        private static string GetUserFriendlyMessage(string message)
        {
            foreach (var key in Messages.Keys)
            {
                if (message.Contains(key))
                {
                    return Messages[key];
                }
            }
            return "Cannot insert/update duplicate record";
        }
    }
}
