﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Core.AutoMapper;
using SMCi.DataAccess.Entities;

namespace SMCi.WebCore.Modules.Posts.Models
{
    public class PostModel : IMapping<Post>
    {
        [HiddenInput]
        public int Id { get; set; }

        [Required, StringLength(255)]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        [UIHint("App/FilePicker")]
        public string Image { get; set; }
    }
}
