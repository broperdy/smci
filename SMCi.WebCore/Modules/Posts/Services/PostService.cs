﻿using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using SMCi.DataAccess.Entities;
using SMCi.DataAccess.Repositories;
using SMCi.WebCore.Modules.Posts.Models;

namespace SMCi.WebCore.Modules.Posts.Services
{
    public interface IPostService
    {
        IQueryable<PostModel> GetAll();
        Task<PostModel> Get(int id);
        Task<Post> Create(PostModel model);
        Task<Post> Update(PostModel model);
        Task Delete(int id);
    }

    public class PostService : IPostService
    {
        private readonly IUnitOfWork _unitOfWork;

        private PostRepository PostRepo => _unitOfWork.Repo<PostRepository>();

        public PostService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<PostModel> GetAll()
        {
            return PostRepo.Queryable().ProjectTo<PostModel>();
        }

        public async Task<PostModel> Get(int id)
        {
            return (await PostRepo.GetAsync(id)).MapTo<PostModel>();
        }

        public async Task<Post> Create(PostModel model)
        {
            var entity = model.MapTo<Post>();
            PostRepo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();

            return entity;
        }

        public async Task<Post> Update(PostModel model)
        {
            var entity = await PostRepo.GetAsync(model.Id);
            if (entity != null)
            {
                model.MapTo(entity);
                await _unitOfWork.SaveChangesAsync();
            }

            return entity;
        }

        public async Task Delete(int id)
        {
            var entity = await PostRepo.GetAsync(id);
            if (entity != null)
            {
                PostRepo.Delete(entity);
                await _unitOfWork.SaveChangesAsync();
            }
        }
    }
}
