﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Core.AutoMapper;
using SMCi.DataAccess.Entities;
using SMCi.DataAccess.Enums;

namespace SMCi.WebCore.Modules.VanBanPhapQuys.Models
{
    public class VanBanPhapQuyModel : IMapping<VanBanPhapQuy>
    {
        [HiddenInput]
        public int Id { get; set; }

        [Required]
        public string TieuDe { get; set; }

        [AllowHtml, UIHint("HtmlEditor")]
        public string NoiDung { get; set; }

        [UIHint("App/FilePicker")]
        public string DocUrl { get; set; }

        public string GhiChu { get; set; }

        [UIHint("App/iStatus")]
        public EnumiStatus iStatus { get; set; }

        public bool IsPublish { get; set; } = true;

        public int DisplayOrder { get; set; }
    }
}
