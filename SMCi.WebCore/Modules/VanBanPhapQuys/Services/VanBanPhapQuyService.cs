﻿using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using SMCi.DataAccess.Entities;
using SMCi.DataAccess.Repositories;
using SMCi.WebCore.Modules.VanBanPhapQuys.Models;

namespace SMCi.WebCore.Modules.VanBanPhapQuys.Services
{
    public interface IVanBanPhapQuyService
    {
        IQueryable<VanBanPhapQuyModel> GetAll();
        Task<VanBanPhapQuyModel> Get(int id);
        Task<VanBanPhapQuy> Create(VanBanPhapQuyModel model);
        Task<VanBanPhapQuy> Update(VanBanPhapQuyModel model);
        Task Delete(int id);
    }

    public class VanBanPhapQuyService : IVanBanPhapQuyService
    {
        private readonly IUnitOfWork _unitOfWork;

        private VanBanPhapQuyRepository VanBanRepo => _unitOfWork.Repo<VanBanPhapQuyRepository>();

        public VanBanPhapQuyService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<VanBanPhapQuyModel> GetAll()
        {
            return VanBanRepo.Queryable().ProjectTo<VanBanPhapQuyModel>();
        }

        public async Task<VanBanPhapQuyModel> Get(int id)
        {
            return (await VanBanRepo.GetAsync(id)).MapTo<VanBanPhapQuyModel>();
        }

        public async Task<VanBanPhapQuy> Create(VanBanPhapQuyModel model)
        {
            var entity = model.MapTo<VanBanPhapQuy>();
            VanBanRepo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();

            return entity;
        }

        public async Task<VanBanPhapQuy> Update(VanBanPhapQuyModel model)
        {
            var entity = await VanBanRepo.GetAsync(model.Id);
            if (entity != null)
            {
                model.MapTo(entity);
                await _unitOfWork.SaveChangesAsync();
            }

            return entity;
        }

        public async Task Delete(int id)
        {
            var entity = await VanBanRepo.GetAsync(id);
            if (entity != null)
            {
                VanBanRepo.Delete(entity);
                await _unitOfWork.SaveChangesAsync();
            }
        }
    }
}
