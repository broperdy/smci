﻿using System.Threading.Tasks;
using SMCi.DataAccess.AspNetIdentity;
using SMCi.WebCore.AspNetIdentity;
using SMCi.WebCore.Modules.Profile.Models;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using Microsoft.AspNet.Identity;

namespace SMCi.WebCore.Modules.Profile.Services
{
    public interface IProfileService
    {
        Task<IdentityResult> UpdateAvatar(string filePath);
        Task<IdentityResult> ChangePassword(ChangePasswordModel model);
        Task<PersonalInfoModel> GetPersonalInfo();
        Task<IdentityResult> UpdatePersonalInfo(PersonalInfoModel model);
    }

    public class ProfileService : IProfileService
    {
        private readonly ICurrentUser _currentUser;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager _userManager;

        public ProfileService(
            ICurrentUser currentUser,
            IUnitOfWork unitOfWork,
            UserManager userManager)
        {
            _currentUser = currentUser;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        public async Task<PersonalInfoModel> GetPersonalInfo()
        {
            return (await _userManager.FindByIdAsync(_currentUser.Id)).MapTo<PersonalInfoModel>();
        }

        public async Task<IdentityResult> UpdatePersonalInfo(PersonalInfoModel model)
        {
            var user = await _userManager.FindByIdAsync(_currentUser.Id);
            model.MapTo(user);

            var result = await _userManager.UpdateAsync(user);

            user.MapTo(_currentUser.Info);

            return result;
        }

        public async Task<IdentityResult> UpdateAvatar(string filePath)
        {
            var user = await _userManager.FindByIdAsync(_currentUser.Id);
            user.Avatar = filePath;

            var result = await _userManager.UpdateAsync(user);
            user.MapTo(_currentUser.Info);

            return result;
        }

        public async Task<IdentityResult> ChangePassword(ChangePasswordModel model)
        {
            return await _userManager.ChangePasswordAsync(_currentUser.Id, model.OldPassword, model.NewPassword);
        }
    }
}
