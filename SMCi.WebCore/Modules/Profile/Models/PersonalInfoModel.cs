﻿using System.ComponentModel.DataAnnotations;
using SMCi.DataAccess.Entities;
using Core.AutoMapper;

namespace SMCi.WebCore.Modules.Profile.Models
{
    public class PersonalInfoModel : IMapping<User>
    {
        [Required]
        public string Email { get; set; }

        [Phone]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [StringLength(100)]
        public string JobTitle { get; set; }

        [StringLength(100)]
        public string Department { get; set; }

        [StringLength(255)]
        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }
    }
}
