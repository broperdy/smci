﻿using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using SMCi.DataAccess.Entities;
using SMCi.DataAccess.Repositories;
using SMCi.WebCore.Modules.ThongTinTiepNhans.Models;

namespace SMCi.WebCore.Modules.ThongTinTiepNhans.Services
{
    public interface IThongTinTiepNhanService
    {
        IQueryable<ThongTinTiepNhanModel> GetAll();
        Task<ThongTinTiepNhanModel> Get(int id);
        Task<ThongTinTiepNhan> Create(ThongTinTiepNhanModel model);
        Task<ThongTinTiepNhan> Update(ThongTinTiepNhanModel model);
        Task Delete(int id);
    }

    public class ThongTinTiepNhanService : IThongTinTiepNhanService
    {
        private readonly IUnitOfWork _unitOfWork;

        private ThongTinTiepNhanRepository ThongTinRepo => _unitOfWork.Repo<ThongTinTiepNhanRepository>();

        public ThongTinTiepNhanService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<ThongTinTiepNhanModel> GetAll()
        {
            return ThongTinRepo.Queryable().ProjectTo<ThongTinTiepNhanModel>();
        }

        public async Task<ThongTinTiepNhanModel> Get(int id)
        {
            return (await ThongTinRepo.GetAsync(id)).MapTo<ThongTinTiepNhanModel>();
        }

        public async Task<ThongTinTiepNhan> Create(ThongTinTiepNhanModel model)
        {
            var entity = model.MapTo<ThongTinTiepNhan>();
            ThongTinRepo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();

            return entity;
        }

        public async Task<ThongTinTiepNhan> Update(ThongTinTiepNhanModel model)
        {
            var entity = await ThongTinRepo.GetAsync(model.Id);
            if (entity != null)
            {
                model.MapTo(entity);
                await _unitOfWork.SaveChangesAsync();
            }

            return entity;
        }

        public async Task Delete(int id)
        {
            var entity = await ThongTinRepo.GetAsync(id);
            if (entity != null)
            {
                ThongTinRepo.Delete(entity);
                await _unitOfWork.SaveChangesAsync();
            }
        }
    }
}
