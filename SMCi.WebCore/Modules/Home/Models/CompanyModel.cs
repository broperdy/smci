﻿using AutoMapper;
using Core.AutoMapper;
using SMCi.DataAccess.Entities;

namespace SMCi.WebCore.Modules.Home.Models
{
    public class CompanyModel : ICustomMappings
    {
        public int CompanyID { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName1 { get; set; }
        public string CompanyAddr { get; set; }
        public int ProvinceID { get; set; }
        public int DistrictID { get; set; }
        public int TownID { get; set; }
        public string DistrictName { get; set; }
        public string RegisterDate { get; set; }
        public string RegisterPlace { get; set; }
        public string CompanyTel { get; set; }
        public string CompanyMail { get; set; }
        public string CompanyWebsite { get; set; }
        public string TaxCode { get; set; }
        public string CompanyLogo { get; set; }
        public int IsParent { get; set; }
        public int CompanyStatus { get; set; }
        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Company, CompanyModel>()
                .AfterMap((src, dest) =>
                {
                    dest.DistrictName = src.District?.DistrictName;
                });
        }
    }
}
