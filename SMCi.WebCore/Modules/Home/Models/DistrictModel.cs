﻿using Core.AutoMapper;
using SMCi.DataAccess.Entities;

namespace SMCi.WebCore.Modules.Home.Models
{
    public class DistrictModel : IMapping<District>
    {
        public int DistrictID { get; set; }
        public int ProvinceID { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public int DistrictStatus { get; set; }
        public int DisplayOrder { get; set; }
        public int CompanyCount { get; set; }
    }
}
