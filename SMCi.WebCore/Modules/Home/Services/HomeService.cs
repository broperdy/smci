﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using SMCi.DataAccess.Entities;
using SMCi.DataAccess.Enums;
using SMCi.DataAccess.Repositories;
using SMCi.WebCore.Modules.Home.Models;

namespace SMCi.WebCore.Modules.Home.Services
{
    public interface IHomeService
    {
        List<DistrictModel> GetDistricts();
        List<Town> GetTowns(int districtId);
        List<Company> GetCompanies(int districtId = 0, int townId = 0);
        Task<List<CompanyModel>> SearchCompany(string search, int districtId, int townId, int pageIndex = 0);
        Company GetCompany(int companyId);
        Task<List<VanBanPhapQuy>> SearchDocuments(string search, int catId, int pageIndex = 0);
        Task<List<ThongTinTiepNhan>> SearchFeedback(string search, int pageIndex = 0);
        Task<int> SaveFeedback(ThongTinTiepNhan model);
    }

    public class HomeService : IHomeService
    {
        private readonly IUnitOfWork _unitOfWork;

        private DistrictRepository DistrictRepo => _unitOfWork.Repo<DistrictRepository>();
        private TownRepository TownRepo => _unitOfWork.Repo<TownRepository>();
        private CompanyRepository CompanyRepo => _unitOfWork.Repo<CompanyRepository>();
        private VanBanPhapQuyRepository DocRepo => _unitOfWork.Repo<VanBanPhapQuyRepository>();
        private ThongTinTiepNhanRepository FeedbackRepo => _unitOfWork.Repo<ThongTinTiepNhanRepository>();

        public HomeService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // Company's functions
        public List<DistrictModel> GetDistricts()
        {
            // Bắc Ninh ProvinceID = 18
            var result = DistrictRepo.Queryable()
                .Where(x => x.DistrictStatus == 1 && x.ProvinceID == 18)
                .OrderBy(x => x.DisplayOrder).ToList().MapToList<DistrictModel>();

            var listDistrictIds = result.Select(x => x.DistrictID).ToList();
            var numCompanyInDistrist = CompanyRepo.Queryable().Where(x => listDistrictIds.Any(y => y == x.DistrictID))
                .GroupBy(x => x.DistrictID);

            foreach (var distrisct in result)
            {
                var companies = numCompanyInDistrist.FirstOrDefault(x => x.Key == distrisct.DistrictID);
                if (companies != null)
                {
                    distrisct.CompanyCount = companies.Count();
                }
            }
            return result;
        }

        public List<Town> GetTowns(int districtId)
        {
            return TownRepo.Queryable()
                .Where(x => x.TownStatus == 1 && x.DistrictID == districtId)
                .OrderBy(x => x.DisplayOrder).ToList();
        }

        public List<Company> GetCompanies(int districtId = 0, int townId = 0)
        {
            var query = CompanyRepo.Queryable();
            if (districtId != 0)
            {
                query = query.Where(x => x.DistrictID == districtId);
            }
            if (townId != 0)
            {
                query = query.Where(x => x.TownID == townId);
            }

            return query.OrderByDescending(x => x.RegisterDate).ToList();
        }

        public Company GetCompany(int companyId)
        {
            return CompanyRepo.Queryable().FirstOrDefault(x => x.CompanyID == companyId);
        }

        public async Task<List<CompanyModel>> SearchCompany(string search, int districtId, int townId, int pageIndex = 0)
        {
            var query = CompanyRepo.Queryable().Where(x => x.IsParent == 0 && x.CompanyStatus == 1);
            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.CompanyName1.ToLower().Contains(search.ToLower()));
            }
            if (districtId != -1)
            {
                query = query.Where(x => x.DistrictID == districtId);
            }
            if (townId != -1)
            {
                query = query.Where(x => x.TownID == townId);
            }

            return (await query.OrderByDescending(x => x.RegisterDate).Skip(10*pageIndex).Take(10).ToListAsync()).MapToList<CompanyModel>();
        }

        // Document's functions
        public async Task<List<VanBanPhapQuy>> SearchDocuments(string search, int catId, int pageIndex = 0)
        {
            var query = DocRepo.Queryable().Where(x => x.IsPublish);
            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.TieuDe.ToLower().Contains(search.ToLower()));
            }

            return await query.OrderByDescending(x => x.DisplayOrder).Skip(10*pageIndex).Take(10).ToListAsync();
        }

        // Thong Tin Tiep Nhan's functions
        public async Task<List<ThongTinTiepNhan>> SearchFeedback(string search, int pageIndex = 0)
        {
            var query = FeedbackRepo.Queryable();
            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.TieuDe.ToLower().Contains(search.ToLower()));
            }

            return await query.OrderByDescending(x => x.Id).Skip(10 * pageIndex).Take(10).ToListAsync();
        }

        public async Task<int> SaveFeedback(ThongTinTiepNhan model)
        {
            FeedbackRepo.Insert(model);
            await _unitOfWork.SaveChangesAsync();

            return model.Id;
        }
    }
}
