﻿using System;
using SMCi.DataAccess.Entities;
using Core.AutoMapper;
using Core.Timing;
using Core.Web.MetadataModel.MetadataAware;

namespace SMCi.WebCore.Modules.Host.Models
{
    public class HostUserPreview : IMapping<User>
    {
        public int Id { get; set; }

        public bool IsLocked => LockoutEnabled && LockoutEndDateUtc.HasValue && LockoutEndDateUtc.Value.Date > Clock.Now.Date;

        public string Email { get; set; }

        public string UserName { get; set; }
        [NotRender]
        public bool LockoutEnabled { get; set; }

        [NotRender]
        public DateTime? LockoutEndDateUtc { get; set; }
    }
}
