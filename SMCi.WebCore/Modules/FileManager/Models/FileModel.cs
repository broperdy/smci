﻿using System;
using System.IO;
using Core.AutoMapper;
using Humanizer;

namespace SMCi.WebCore.Modules.FileManager.Models
{
    public class FileModel : IMapFrom<FileInfo>
    {
        public string Name { get; set; }
        public long Length { get; set; }
        public string Extension { get; set; }
        public string Path { get; set; }
        public DateTime CreationTime { get; set; }
        public string AddedDateString => CreationTime.Humanize(false);
    }
}
