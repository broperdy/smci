﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Core.Web.MetadataModel.Validations;

namespace SMCi.WebCore.Modules.FileManager.Models
{
    public class UploadedFileModel
    {
        [HiddenInput]
        [Required]
        public string TargetId { get; set; }

        [FileUpload]
        public HttpPostedFileBase FileUpload { get; set; }
    }
}