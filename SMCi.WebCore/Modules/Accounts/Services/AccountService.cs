﻿using System;
using System.Threading.Tasks;
using System.Web;
using SMCi.DataAccess.AspNetIdentity;
using SMCi.DataAccess.AspNetIdentity.Extensions;
using SMCi.DataAccess.Entities;
using SMCi.DataAccess.Enums;
using SMCi.WebCore.AspNetIdentity;
using SMCi.WebCore.Extensions;
using SMCi.WebCore.Modules.Accounts.Models;
using Core.AutoMapper;
using Core.Configuration;
using Core.Net.Mail.Template;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace SMCi.WebCore.Modules.Accounts.Services
{
    public interface IAccountService
    {
        Task<SignInStatus> Login(LoginModel model);
        Task<IdentityResult> Register(AccountModel model);
        Task<IdentityResult> ConfirmEmail(int userId, string code);
        Task<User> ForgotPassword(ForgotPasswordModel model);
        Task<IdentityResult> ResetPassword(ResetPasswordModel model);
        Task SendConfirmEmail(int userId, string email);
        Task<User> FindUserBy(string username, string password);
    }

    public class AccountService : IAccountService
    {
        private readonly SignInManager _signInManager;
        private readonly ISettingManager _settingManager;
        private readonly ITemplateEmailSender _emailSender;
        private readonly UserManager _userManager;

        public AccountService(
            UserManager userManager,
            SignInManager signInManager,
            ISettingManager settingManager,
            ITemplateEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _settingManager = settingManager;
            _emailSender = emailSender;
        }

        public async Task<User> FindUserBy(string username, string password)
        {
            return await _userManager.FindAsync(username, password);
        }

        public Task<SignInStatus> Login(LoginModel model)
        {
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            return _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, shouldLockout: false);
        }

        public async Task<IdentityResult> Register(AccountModel model)
        {
            var user = model.MapTo<User>();
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, ERole.Customer);
                // Update Activation Code with user registered Id
                await SendConfirmEmail(user.Id, user.Email);
            }

            return result;
        }

        public async Task<IdentityResult> ConfirmEmail(int userId, string code)
        {
            return await _userManager.ConfirmEmailAsync(userId, code);
        }

        public async Task<User> ForgotPassword(ForgotPasswordModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null) return null;

            var code = await _userManager.GeneratePasswordResetTokenAsync(user.Id);

            //Set request domain to web app domain
            var requestUrl = $"{_settingManager.ResetPasswordUrl()}?userId={user.Id}&code={HttpUtility.UrlEncode(code)}";
            await _emailSender.SendForgotPassword(user.Email, new Uri(requestUrl).AbsoluteUri);

            return user;
        }

        public async Task<IdentityResult> ResetPassword(ResetPasswordModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return new IdentityResult("The email does not exist");
            }

            return await _userManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
        }

        public async Task SendConfirmEmail(int userId, string email)
        {
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(userId);

            var requestUrl = $"{_settingManager.ConfirmEmailUrl()}?userId={userId}&code={HttpUtility.UrlEncode(code)}";
            await _emailSender.SendConfirmEmail(email, new Uri(requestUrl).AbsoluteUri);
        }
    }
}
