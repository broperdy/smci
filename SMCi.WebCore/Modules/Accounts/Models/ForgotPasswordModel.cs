﻿using System.ComponentModel.DataAnnotations;

namespace SMCi.WebCore.Modules.Accounts.Models
{
    public class ForgotPasswordModel
    {
        [Required]
        [EmailAddress, StringLength(255)]
        public string Email { get; set; }
    }
}
