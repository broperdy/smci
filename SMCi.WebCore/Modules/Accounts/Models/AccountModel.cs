﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AutoMapper;
using SMCi.DataAccess.Entities;
using Core.AutoMapper;
using Core.Web.MetadataModel.Validations;

namespace SMCi.WebCore.Modules.Accounts.Models
{
    public class AccountModel : ICustomMappings
    {
        [HiddenInput]
        public string ActivationCode { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        public string ConfirmPassword { get; set; }

        [BooleanRequired]
        public bool AgreeTerms { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<AccountModel, User>();
        }
    }
}