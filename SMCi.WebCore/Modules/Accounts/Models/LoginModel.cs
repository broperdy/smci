﻿using System.ComponentModel.DataAnnotations;

namespace SMCi.WebCore.Modules.Accounts.Models
{
    public class LoginModel
    {
        [Required]
        [StringLength(255)]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
