﻿namespace SMCi.WebCore.Helpers
{
    public enum EiStatus
    {
        Pending,
        Accepted,
        Rejected
    }
}
