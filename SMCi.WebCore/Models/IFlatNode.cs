﻿namespace SMCi.WebCore.Models
{
    public interface IFlatNode
    {
        int Id { get; set; }
        int? ParentId { get; set; }
        int SortOrder { get; set; }
        int Level { get; set; }
        string Text { get; set; }
        bool HasChildren { get; set; }
    }
}
