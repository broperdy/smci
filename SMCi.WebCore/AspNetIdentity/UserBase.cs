using System.Collections.Generic;
using System.Linq;
using SMCi.DataAccess.AspNetIdentity;
using SMCi.DataAccess.Enums;
using Core.AutoMapper;
using Core.Runtime.Session;
using Microsoft.AspNet.Identity;

namespace SMCi.WebCore.AspNetIdentity
{
    public abstract class UserBase
    {
        protected readonly UserManager UserManager;

        public int Id => UserSessions.UserId.GetValueOrDefault();
        public int TenantId => UserSessions.TenantId.GetValueOrDefault();
        public IList<string> Roles => UserSessions.Roles;

        public LoggedInUser Info => LoggedInUser;

        protected abstract LoggedInUser LoggedInUser { get; set; }

        protected UserBase(UserManager userManager)
        {
            UserManager = userManager;
        }

        public bool IsInRole(params ERole[] roles)
        {
            return roles.Any(role => Roles.Contains(role.ToString()));
        }

        protected LoggedInUser CreateLoggedInUser()
        {
            var loggedInUser = new LoggedInUser();
            var user = UserManager.FindById(Id);
            user?.MapTo(loggedInUser);
            return loggedInUser;
        }
    }
}