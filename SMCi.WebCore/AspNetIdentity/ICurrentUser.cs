﻿using System.Collections.Generic;
using SMCi.DataAccess.Enums;

namespace SMCi.WebCore.AspNetIdentity
{
    public interface ICurrentUser
    {
        int Id { get; }
        int TenantId { get; }
        LoggedInUser Info { get; }
        IList<string> Roles { get; }
        bool IsInRole(params ERole[] roles);
        void Refresh();
    }
}