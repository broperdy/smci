﻿using System;
using SMCi.DataAccess.AspNetIdentity;
using SMCi.DataAccess.Entities;
using SMCi.WebCore.AspNetIdentity.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using Owin;

namespace SMCi.WebCore.AspNetIdentity
{
    public static class UserManagerConfigurator
    {
        public static UserManager Configure(this UserManager userManager, IAppBuilder app)
        {
            // Configure validation logic for usernames
            userManager.UserValidator = new UserValidator<User, int>(userManager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            userManager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false
            };

            // Configure user lockout defaults
            userManager.UserLockoutEnabledByDefault = true;
            userManager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(30);
            userManager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            //RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<User, int>
            //{
            //    MessageFormat = "Your security code is {0}"
            //});

            //RegisterTwoFactorProvider("Username Code", new EmailTokenProvider<User, int>
            //{
            //    Subject = "Security Code",
            //    BodyFormat = "Your security code is {0}"
            //});

            userManager.EmailService = new EmailService();
            //this.SmsService = new SmsService();
            var dataProtectionProvider = app.GetDataProtectionProvider();
            if (dataProtectionProvider != null)
            {
                userManager.UserTokenProvider = new DataProtectorTokenProvider<User, int>(dataProtectionProvider.Create("ASP.NET Identity"));
            }

            return userManager;
        }
    }
}