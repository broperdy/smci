using SMCi.DataAccess.AspNetIdentity;
using Core.Web.Caching;

namespace SMCi.WebCore.AspNetIdentity
{
    public class CurrentUser : UserBase, ICurrentUser
    {
        private readonly ISessionCache _sessionCache;
        private const string CurrentUserKey = "__CurrentUser__";

        public CurrentUser(UserManager userManager, ISessionCache sessionCache)
            : base(userManager)
        {
            _sessionCache = sessionCache;
        }

        public void Refresh()
        {
            _sessionCache.Remove(CurrentUserKey);
        }

        protected override LoggedInUser LoggedInUser
        {
            get
            {
                return _sessionCache.GetOrAdd(CurrentUserKey, CreateLoggedInUser);
            }
            set
            {
                _sessionCache.Set(CurrentUserKey, value);
            }
        }
    }
}