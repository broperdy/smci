﻿using SMCi.Rest.Zendesk;
using SMCi.Rest.Zendesk.Dtos;

namespace SMCi.WebCore.Services
{
    public interface IZendeskManagerService
    {
        void PostTicket(NewTicketDto ticket);
    }

    public class ZendeskManagerService : IZendeskManagerService
    {
        private readonly IZendeskRestContext _zendeskRest;

        public ZendeskManagerService(IZendeskRestContext zendeskRest)
        {
            _zendeskRest = zendeskRest;
        }

        public void PostTicket(NewTicketDto ticket)
        {
            _zendeskRest.Requests.PostRequest(ticket);
        }
    }
}
