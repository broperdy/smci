﻿using System;
using System.Linq;
using System.Threading.Tasks;
using SMCi.DataAccess.AspNetIdentity;
using SMCi.DataAccess.AspNetIdentity.Extensions;
using SMCi.DataAccess.Entities;
using SMCi.DataAccess.Enums;
using SMCi.DataAccess.Repositories;
using Core.DataAccess.Uow;
using Core.Tasks;

namespace SMCi.WebCore.Tasks
{
    public class SystemInitializer : OrderedTask, IRunAtInit
    {
        private readonly RoleManager _roleManager;
        private readonly UserManager _userManager;
        private readonly IUnitOfWork _unitOfWork;

        public SystemInitializer(RoleManager roleManager, UserManager userManager, IUnitOfWork unitOfWork)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _unitOfWork = unitOfWork;
        }

        public async Task Execute()
        {
            await CreateRoles();
            await CreateUserHost();
        }

        public async Task CreateRoles()
        {
            var allRoles = _roleManager.Roles.Select(x => x.Name).ToList();
            var defaults = Enum.GetNames(typeof(ERole));

            var listNotExist = defaults.Except(allRoles).Select(x => new Role
            {
                Name = x
            }).ToList();

            if (!listNotExist.Any()) return;

            foreach (var role in listNotExist)
            {
                await _roleManager.CreateAsync(role);
            }
        }

        private async Task CreateUserHost()
        {
            if (await _unitOfWork.Repo<UserRepository>().GetHostId() > 0) return;

            var user = new User
            {
                UserName = ERole.Host.ToString().ToLower(),
                Email = "admin@gmail.com",
                EmailConfirmed = true
            };

            var result = await _userManager.CreateAsync(user, "zxcvbn");
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, ERole.Host);
            }
        }
    }
}
