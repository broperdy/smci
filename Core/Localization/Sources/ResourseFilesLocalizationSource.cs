﻿using CuttingEdge.Conditions;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;

namespace Core.Localization.Sources
{
    public class ResourseFilesLocalizationSource : ILocalizationSource
    {
        private readonly IList<ResourceManager> _sources;

        public ResourseFilesLocalizationSource(IList<ResourceManager> sources)
        {
            Condition.Ensures(sources).IsNotNull().IsNotEmpty();
            _sources = sources;
        }

        public string GetString(string name)
        {
            return GetNullableString(name, null, allowNull: false);
        }

        public string GetString(string name, CultureInfo cultureInfo)
        {
            return GetNullableString(name, cultureInfo, allowNull: false);
        }

        public string GetStringOrNull(string name, CultureInfo cultureInfo)
        {
            return GetNullableString(name, cultureInfo);
        }

        public string GetStringOrNull(string name, string fileName = "")
        {
            return GetNullableString(name, null, true, fileName);
        }

        private string GetNullableString(string name, CultureInfo cultureInfo, bool allowNull = true, string fileName = "")
        {
            string text = null;
            foreach (var source in _sources)
            {
                if (!string.IsNullOrEmpty(fileName) && !source.BaseName.Contains(fileName)) continue;
                text = cultureInfo != null
                    ? source.GetString(name, cultureInfo)
                    : source.GetString(name);

                if (text != null) break;
            }

            if (text == null && allowNull)
            {
                return null;
            }

            return text ?? LocalizationSourceHelper.ReturnGivenNameOrThrowException(name);
        }
    }
}