﻿using System.Collections.Generic;

namespace Core.Localization
{
    public interface ILanguageManager
    {
        LanguageInfo CurrentLanguage { get; }
        IReadOnlyList<LanguageInfo> GetLanguages();
    }
}