using Core.Localization.Sources;

namespace Core.Localization
{
    public class LocalizationConfig : ILocalizationConfig
    {
        public ILocalizationSource Source { get; set; }
        public ILanguageProvider LanguageProvider { get; }
        public bool IsEnabled { get; set; }
        public bool ReturnGivenTextIfNotFound { get; set; }
        public bool WrapGivenTextIfNotFound { get; set; }
        public bool TitleizeGivenTextIfNotFound { get; set; }

        public LocalizationConfig(ILocalizationSource source, ILanguageProvider languageProvider)
        {
            LanguageProvider = languageProvider;
            Source = source;
            IsEnabled = true;
            ReturnGivenTextIfNotFound = false;
            WrapGivenTextIfNotFound = false;
            TitleizeGivenTextIfNotFound = true;
        }
    }
}