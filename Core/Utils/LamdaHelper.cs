﻿using System;
using System.Linq.Expressions;

namespace Core.Utils
{
    public class LamdaHelper
    {
        public static string PropertyName<T>(Expression<Func<T, object>> expression)
        {
            var body = expression.Body as MemberExpression ??
                       ((UnaryExpression)expression.Body).Operand as MemberExpression;

            return body.Member.Name;
        }

        public static string PropertyName<TModel, TProp>(Expression<Func<TModel, TProp>> expression)
        {
            var body = expression.Body as MemberExpression ??
                       ((UnaryExpression)expression.Body).Operand as MemberExpression;

            return body.Member.Name;
        }
    }
}