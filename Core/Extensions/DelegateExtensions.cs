﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Core.Extensions
{
    public delegate T ObjectActivator<T>(params object[] args);

    public static class DelegateExtensions
    {
        public static Delegate GetCompileConstructor<T>()
        {
            return GetCompileConstructor<T>(typeof(T).GetConstructors().First());
        }

        private static Delegate GetCompileConstructor<T>(ConstructorInfo ctor)
        {
            var paramsInfo = ctor.GetParameters();
            var param = Expression.Parameter(typeof(object[]), "args");
            var argsExp = new Expression[paramsInfo.Length];

            for (var i = 0; i < paramsInfo.Length; i++)
            {
                var index = Expression.Constant(i);
                var paramType = paramsInfo[i].ParameterType;

                var paramAccessorExp = Expression.ArrayIndex(param, index);
                var paramCastExp = Expression.Convert(paramAccessorExp, paramType);
                argsExp[i] = paramCastExp;
            }

            var newExp = Expression.New(ctor, argsExp);
            return Expression.Lambda(typeof(ObjectActivator<T>), newExp, param).Compile();
        }
    }
}
