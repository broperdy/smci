﻿using Core.Infrastructure.Objects;
using Core.Localization;
using Core.Localization.Sources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Core.Extensions
{
    public static class EnumExtensions
    {
        public static T? ToEnum<T>(this object obj)
            where T : struct
        {
            T result;
            if (obj != null && Enum.TryParse(obj.ToString(), out result))
            {
                return result;
            }
            return null;
        }

        public static T ToEnum<T>(this object obj, T defaultValue)
            where T : struct
        {
            var result = obj.ToEnum<T>();
            return result == null ? defaultValue : result.Value;
        }

        public static bool IsEnumType<T>(this object obj)
            where T : struct
        {
            return ToEnum<T>(obj) != null;
        }

        public static string Description(this Enum obj)
        {
            var type = obj.GetType();
            var memInfo = type.GetMember(obj.ToString());
            if (memInfo.Length > 0)
            {
                var attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }
            return obj.ToString();
        }

        public static IList<PairInt> ToEnumPairInt(this Type type, ILocalizationConfig config = null)
        {
            var values = Enum.GetValues(type);

            var list = new List<PairInt>();
            foreach (var value in values)
            {
                var item = new PairInt
                {
                    Value = (int)value,
                    Text = Enum.GetName(type, value)
                };

                if (config != null && config.IsEnabled)
                {
                    item.Text = config.Source.GetModelString(type, item.Text);
                }

                list.Add(item);
            }
            return list;
        }

        public static IList<PairString> ToEnumPairString(this Type type, ILocalizationConfig config = null)
        {
            var names = Enum.GetNames(type);
            var list = new List<PairString>();

            foreach (var name in names)
            {
                var item = new PairString
                {
                    Value = name,
                    Text = name
                };

                if (config != null && config.IsEnabled)
                {
                    item.Text = config.Source.GetModelString(type, item.Text);
                }

                list.Add(item);
            }
            return list;
        }
    }
}