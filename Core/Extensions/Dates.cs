﻿using System;

namespace Core.Extensions
{
    public static class Dates
    {
        public static DateTime BeginOfTheYear(this DateTime date)
        {
            return new DateTime(date.Year, 1, 1);
        }

        public static DateTime BeginOfTheMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        public static DateTime EndOfTheMonth(this DateTime date)
        {
            var endOfTheMonth = new DateTime(date.Year, date.Month, 1)
                .AddMonths(1)
                .AddDays(-1);

            return endOfTheMonth;
        }

        public static string ToDateString(this DateTime? date)
        {
            return date?.ToString("yyyy-MM-dd");
        }

        public static string ToDateTimeString(this DateTime? date)
        {
            return date?.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string ToDateString(this DateTime date, string separator = "-")
        {
            return date.ToString($"yyyy{separator}MM{separator}dd");
        }

        public static string ToDateTimeString(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}
