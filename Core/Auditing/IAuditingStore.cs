﻿using System.Threading.Tasks;

namespace Core.Auditing
{
    public interface IAuditingStore
    {
        Task SaveAsync(AuditInfo auditInfo);
    }
}