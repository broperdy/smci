using Serilog;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Core.Net.Mail
{
    /// <summary>
    /// This class is an implementation of <see cref="IEmailSender"/> as similar to null pattern.
    /// It does not send emails but logs them.
    /// </summary>
    public class NullEmailSender : EmailSenderBase
    {
        private readonly ILogger _logger;

        /// <summary>
        /// Creates a new <see cref="NullEmailSender"/> object.
        /// </summary>
        /// <param name="config">Configuration</param>
        public NullEmailSender(IEmailSenderConfig config)
            : base(config)
        {
            _logger = Log.ForContext<NullEmailSender>();
        }

        protected override Task SendEmailAsync(MailMessage mail)
        {
            _logger.Debug("SendEmailAsync:");
            LogEmail(mail);
            return Task.FromResult(0);
        }

        protected override void SendEmail(MailMessage mail)
        {
            _logger.Debug("SendEmail:");
            LogEmail(mail);
        }

        private void LogEmail(MailMessage mail)
        {
            _logger.Debug(mail.To.ToString());
            _logger.Debug(mail.CC.ToString());
            _logger.Debug(mail.Subject);
            _logger.Debug(mail.Body);
        }
    }
}