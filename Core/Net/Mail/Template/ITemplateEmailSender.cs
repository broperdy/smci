﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Net.Mail.Template
{
    public interface ITemplateEmailSender
    {
        Task SendEmailAsync(string template, string to, string subject, IDictionary<string, string> replacements = null);
    }
}