﻿using Core.Extensions;
using Core.Runtime.Security;
using System.Linq;
using System.Security.Claims;
using System.Threading;

namespace Core.Runtime.Session
{
    public class ClaimsUserSession : IUserSession
    {
        public int? TenantId => GetClaimValue(CustomClaimTypes.TenantId);

        public int? ImpersonatorTenantId => GetClaimValue(CustomClaimTypes.ImpersonatorTenantId);

        public virtual int? UserId => GetClaimValue(ClaimTypes.NameIdentifier);

        public int? ImpersonatorUserId => GetClaimValue(CustomClaimTypes.ImpersonatorUserId);

        public string[] Roles
        {
            get
            {
                var claimsPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
                var claimsIdentity = claimsPrincipal?.Identity as ClaimsIdentity;
                return claimsIdentity?.Claims.Where(c => c.Type == ClaimTypes.Role).Select(x => x.Value).ToArray() ?? new string[0];
            }
        }

        private static int? GetClaimValue(string claimType)
        {
            var claimsPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
            if (claimsPrincipal == null)
            {
                return null;
            }

            var claimsIdentity = claimsPrincipal.Identity as ClaimsIdentity;
            var value = claimsIdentity?.Claims.FirstOrDefault(c => c.Type == claimType);
            return value?.Value.ChangeTypeTo<int>(default(int));
        }
    }
}