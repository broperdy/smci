﻿using Core.Web.MetadataModel.Validations;
using Kendo.Mvc.UI.Fluent;

namespace Core.Web.Kendo
{
    public static class UploadExtension
    {
        public static UploadValidationSettingsBuilder MaxFileSizeInMB(this UploadValidationSettingsBuilder builder, int fileSize)
        {
            return builder.MaxFileSize(fileSize * 1024 * 1024);
        }

        public static UploadValidationSettingsBuilder AllowedImageExtensions(this UploadValidationSettingsBuilder builder)
        {
            return builder.AllowedExtensions(ImageUploadAttribute.DefaultExtensions);
        }
    }
}
