﻿using System.Linq;
using Core.Dependency.Extensions;
using Core.Tasks;

namespace Core.Dependency.Tasks
{
    public static class ScopeTaskCommand
    {
        public static async void ExecuteInitTasks()
        {
            using (var scope = DependencyManager.Current.BeginScope())
            {
                foreach (var task in scope.Container.GetAllInstances<IRunAtInit>().OrderBy(x => x.Order))
                {
                    await task.Execute();
                }
            }
        }
    }
}
