﻿using System;
using Core.Auditing;
using Core.Configuration;
using Core.Configuration.Startup;
using Core.Localization;
using Core.Localization.Sources;
using Core.Net.Mail.Smtp;
using Core.Runtime.Session;
using SimpleInjector;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using Core.Dependency.Fake;
using Core.Net.Mail;
using Core.Net.Mail.Template;
using Core.Web.Caching;
using Core.Web.MetadataModel;
using Core.Web.MetadataModel.Filters;
using SimpleInjector.Lifestyles;

namespace Core.Dependency.Extensions
{
    public static class ContainerExtensions
    {
        [DebuggerStepThrough]
        public static void RegisterLocalization(this Container container, IList<ResourceManager> sources, IList<LanguageInfo> languages = null)
        {
            container.RegisterSingleton<ILocalizationConfig, LocalizationConfig>();
            container.RegisterSingleton<ILanguageManager, LanguageManager>();

            if (languages == null || !languages.Any())
            {
                container.RegisterSingleton<ILanguageProvider>(() => new LanguageProvider().WithDefault());
            }
            else
            {
                container.RegisterSingleton<ILanguageProvider>(() => new LanguageProvider().AddRange(languages));
            }

            container.RegisterSingleton<ILocalizationSource>(() => new ResourseFilesLocalizationSource(sources));
        }

        [DebuggerStepThrough]

        public static void RegisterDependencyManager(this Container container)
        {
            container.RegisterSingleton<IIocManager>(new DefaultIocManager(container));
            DependencyManager.SetCurrent(container.GetInstance<IIocManager>);
        }

        [DebuggerStepThrough]

        public static void RegisterDefaultDepdendencies(this Container container, IList<ResourceManager> sources, IList<LanguageInfo> languages = null)
        {
            container.RegisterSingleton<ISiteConfig, SiteConfig>();
            container.RegisterSingleton<IUserSession, ClaimsUserSession>();
            container.RegisterSingleton<ISmtpEmailSenderConfig, SmtpEmailSenderConfig>();
            container.RegisterSingleton<ISettingManager, FileSettingManager>();
            container.RegisterSingleton<IAuditingConfig, AuditingConfig>();

            container.Register<IEmailSender, SmtpEmailSender>(Lifestyle.Scoped);
            container.Register<ITemplateEmailSender, TemplateEmailSender>(Lifestyle.Scoped);
            container.Register<IEmailTemplateService, EmailTemplateService>(Lifestyle.Scoped);

            container.RegisterCollection<IModelMetadataFilter>(new[] { typeof(IModelMetadataFilter).Assembly });
            container.Register<ModelMetadataProvider, ExtensibleModelMetadataProvider>();

            container.RegisterHttpContext();
            container.RegisterLocalization(sources, languages);
            container.RegisterDependencyManager();
        }

        [DebuggerStepThrough]

        public static void RegisterHttpContext(this Container container)
        {
            container.Register<HttpContextBase>(
                instanceCreator: () =>
                {
                    if (container.IsVerifying) return new FakeHttpContext();
                    return new HttpContextWrapper(HttpContext.Current);
                }, lifestyle: Lifestyle.Scoped);

            container.Register<HttpSessionStateBase>(
                instanceCreator: () =>
                {
                    if (container.IsVerifying) return new FakeHttpSessionState();
                    return new HttpSessionStateWrapper(HttpContext.Current.Session);
                }, lifestyle: Lifestyle.Scoped);

            container.Register<ISessionCache, SessionCache>(Lifestyle.Scoped);
            container.Register<IPerRequestCache, PerRequestCache>(Lifestyle.Scoped);
        }

        [DebuggerStepThrough]

        public static Container SimpleInjectorContainer(this IIocManager manager)
        {
            return manager.GetContainer() as Container;
        }

        [DebuggerStepThrough]
        public static Scope BeginScope(this IIocManager manager)
        {
            return AsyncScopedLifestyle.BeginScope(manager.SimpleInjectorContainer());
        }

        public static void RegisterDefaultConventions(this Container container, string assemblyPrefix, params string[] nsPrefixes)
        {
            var registrations = AppDomain.CurrentDomain.GetAssemblies()
                .Where(x => x.FullName.StartsWith(assemblyPrefix))
                .SelectMany(x => x.GetExportedTypes())
                .Where(type => (!nsPrefixes.Any() ||
                                nsPrefixes.Any(prefix => type.Namespace != null && type.Namespace.StartsWith(prefix))) &&
                                !type.IsInterface &&
                                !type.IsAbstract &&
                                type.GetInterfaces().Any() &&
                                type.Name.EndsWith("Service"))
                .Select(type => new
                {
                    Service = type.GetInterfaces().FirstOrDefault(x => x.Name.EndsWith(type.Name)),
                    Implementation = type
                })
                .Where(x => x.Service != null && x.Service.FullName.StartsWith(assemblyPrefix));

            foreach (var reg in registrations)
            {
                container.Register(reg.Service, reg.Implementation, Lifestyle.Scoped);
            }
        }
    }
}