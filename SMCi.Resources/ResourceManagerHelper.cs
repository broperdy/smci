﻿using System.Collections.Generic;
using System.Resources;

namespace SMCi.Resources
{
    public static class ResourceManagerHelper
    {
        public static IList<ResourceManager> GetAll()
        {
            return new[] {
                ResTexts_en.ResourceManager,
                ResTexts_vi.ResourceManager
            };
        }
    }
}
