﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.Web.Mvc.Alerts;
using Core.Web.Mvc.Filters;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SMCi.WebCore.Modules.Posts.Models;
using SMCi.WebCore.Modules.Posts.Services;
using SMCi.WebUI.Controllers.Base;

namespace SMCi.WebUI.Areas.Admin.Controllers
{
    public class ManagePostController : HostAuthorizedController
    {
        private readonly IPostService _service;

        public ManagePostController(IPostService service)
        {
            _service = service;
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.GetAll().ToDataSourceResult(request));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Destroy([DataSourceRequest] DataSourceRequest request, PostModel model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Delete(model.Id));
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View(new PostModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Create(PostModel model)
        {
            try
            {
                await _service.Create(model);
                return RedirectToAction("Index").WithSuccess("Tạo thành công");
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            return View(await _service.Get(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Edit(PostModel model)
        {
            try
            {
                await _service.Update(model);
                return View(model).WithSuccess("Cập nhật thành công");
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
            return View(model);
        }
    }
}