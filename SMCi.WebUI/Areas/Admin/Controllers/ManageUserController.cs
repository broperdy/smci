﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using SMCi.WebCore.Extensions;
using SMCi.WebCore.Modules.Host.Services;
using SMCi.WebUI.Controllers.Base;
using Core.Web.Mvc.Ajax;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace SMCi.WebUI.Areas.Admin.Controllers
{
    public class ManageUserController : HostAuthorizedController
    {
        private readonly ManageUserService _service;

        public ManageUserController(ManageUserService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.GetList().ToDataSourceResult(request));
        }


        [HttpPost]
        public async Task<JsonResult> ToggleIsLocked(int id)
        {
            try
            {
                var result = await _service.ToggleIsLocked(id);
                if (result.Succeeded)
                {
                    return Json(AjaxResult.Success().ToResult());
                }

                ModelState.AddIdentityResultErrors(result);
                return Json(AjaxResult.Error().WithModelState(ModelState).ToResult());
            }
            catch (Exception ex)
            {
                return Json(AjaxResult.Error().WithException(ex).ToResult());
            }
        }

        [HttpPost]
        public async Task<JsonResult> ResetPassword(int id)
        {
            try
            {
                var result = await _service.ResetPassword(id);
                if (result.Succeeded)
                {
                    return Json(AjaxResult.Success().ToResult());
                }

                ModelState.AddIdentityResultErrors(result);
                return Json(AjaxResult.Error().WithModelState(ModelState).ToResult());
            }
            catch (Exception ex)
            {
                return Json(AjaxResult.Error().WithException(ex).ToResult());
            }
        }
    }
}