﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.Web.Mvc.Alerts;
using Core.Web.Mvc.Filters;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SMCi.WebCore.Modules.VanBanPhapQuys.Models;
using SMCi.WebCore.Modules.VanBanPhapQuys.Services;
using SMCi.WebUI.Controllers.Base;

namespace SMCi.WebUI.Areas.Admin.Controllers
{
    public class ManageVanBanPhapQuyController : HostAuthorizedController
    {
        private readonly IVanBanPhapQuyService _service;

        public ManageVanBanPhapQuyController(IVanBanPhapQuyService service)
        {
            _service = service;
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.GetAll().ToDataSourceResult(request));
        }

        public Task<ActionResult> Grid_Destroy([DataSourceRequest] DataSourceRequest request, VanBanPhapQuyModel model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Delete(model.Id));
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View(new VanBanPhapQuyModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Create(VanBanPhapQuyModel model)
        {
            try
            {
                await _service.Create(model);
                return RedirectToAction("Index").WithSuccess("Tạo thành công");
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            return View(await _service.Get(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Edit(VanBanPhapQuyModel model)
        {
            try
            {
                await _service.Update(model);
                return View(model).WithSuccess("Cập nhật thành công");
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
            return View(model);
        }
    }
}