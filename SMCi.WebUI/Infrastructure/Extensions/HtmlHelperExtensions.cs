﻿using System.Web;
using System.Web.Mvc;
using HtmlTags;

namespace SMCi.WebUI.Infrastructure.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static IHtmlString Spinner(this HtmlHelper helper)
        {
            var tag = new DivTag()
                .AddClass("sk-spinner sk-spinner-three-bounce");

            tag.Children.Add(new DivTag().AddClass("sk-bounce1"));
            tag.Children.Add(new DivTag().AddClass("sk-bounce2"));
            tag.Children.Add(new DivTag().AddClass("sk-bounce3"));

            return new HtmlString(tag.ToHtmlString());
        }

        public static IHtmlString BooleanIcon(this HtmlHelper helper, bool value)
        {
            return value ? new HtmlString("<i class=\"fa fa-check text-navy\"></i>") : new HtmlString("");
        }
    }
}