﻿using System.Collections.Generic;
using System.Linq;
using MvcSiteMapProvider.Web.Html.Models;

namespace SMCi.WebUI.Infrastructure.Extensions
{
    public static class SiteMapExtensions
    {
        public static IEnumerable<SiteMapNodeModel> VisibleNodes(this List<SiteMapNodeModel> list)
        {
            return list.Where(x => !x.Attributes.ContainsKey("hidden"));
        }

        public static string Icon(this SiteMapNodeModel node)
        {
            var icon = "th-large";
            if (node.Attributes.ContainsKey("icon"))
            {
                icon = node.Attributes["icon"].ToString();
            }
            return icon;
        }

        public static bool HasVisibleChildren(this SiteMapNodeModel parent)
        {
            return parent.Children.VisibleNodes().Any();
        }

        public static string NodeUrl(this SiteMapNodeModel node)
        {
            return node.IsClickable ? node.Url : "#";
        }

        private static bool IsSelected(this SiteMapNodeModel node)
        {
            if (node.IsInCurrentPath && node.IsRootNode && !node.IsCurrentNode)
            {
                return false;
            }
            return node.IsInCurrentPath;
        }

        public static string Active(this SiteMapNodeModel node, string cssClass = "active")
        {
            return node.IsSelected() ? cssClass : "";
        }
    }
}