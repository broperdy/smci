﻿using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using HtmlTags;
using MvcSiteMapProvider;

namespace SMCi.WebUI.Infrastructure.Extensions
{
    public static class ViewHelper
    {
        public static string AdminLayout => "~/Views/Shared/_LayoutAdmin.cshtml";

        public static IHtmlString RenderPageScript(string page)
        {
            if (!page.EndsWith(".js"))
            {
                page += ".js";
            }
            return Scripts.Render($"/scripts/app/pages/{page}");
        }

        public static IHtmlString RenderAdminFonts()
        {
            return RenderFonts(
                "Open+Sans:400,300,600,700",
                "Roboto:400,300,500,700");
        }

        public static IHtmlString RenderHomeFonts()
        {
            return RenderFonts(
                "Roboto:400,300,300italic,400italic,500,500italic,700,700italic",
                "Wire+One&v1");
        }

        public static IHtmlString RenderStyles(string type = "admin")
        {
            var sb = new StringBuilder();
            sb.AppendLine(Styles.Render($"~/Content/css/{type}").ToHtmlString());
            return new HtmlString(sb.ToString());
        }

        public static IHtmlString RenderScripts(string type = "admin")
        {
            return Scripts.Render($"~/bundles/js-{type}");
        }

        public static string PageTitle(this HtmlHelper helper)
        {
            var pageTitle = string.IsNullOrEmpty(helper.ViewBag.PageTitle) ? SiteMaps.Current.CurrentNode?.Description : SiteMaps.Current.CurrentNode?.Title;
            return string.Format("{0}SMCi", string.IsNullOrEmpty(pageTitle) ? "" : pageTitle + " - ");
        }

        public static IHtmlString RenderGoogleMapScript(this HtmlHelper helper, string callback)
        {
            var tag = new HtmlTag("script")
                .Attr("src", $"https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCbUdU6HZ_1oV12U4V76kNivfYjhNhvi_g&callback={callback}")
                .Attr("async", "")
                .Attr("defer", "");

            return helper.Raw(tag);
        }

        public static IHtmlString RenderFonts(params string[] fonts)
        {
            var sb = new StringBuilder();
            foreach (var font in fonts)
            {
                sb.AppendLine(CreateLinkFont(font));
            }
            return new HtmlString(sb.ToString());
        }


        private static string CreateLinkFont(string font)
        {
            return new HtmlTag("link")
                 .Attr("href", $"https://fonts.googleapis.com/css?family={font}")
                 .Attr("rel", "stylesheet")
                 .Attr("type", "text/css")
                 .ToHtmlString();
        }
    }
}