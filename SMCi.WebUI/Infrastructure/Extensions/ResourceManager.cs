﻿using System.Web.Mvc;
using Core.Localization.Sources;

namespace SMCi.WebUI.Infrastructure.Extensions
{
    public static class ResourceManager
    {
        private static ILocalizationSource Localization => DependencyResolver.Current.GetService<ILocalizationSource>();

        public static string GetText(string name, string lang)
        {
            return Localization.GetStringOrNull(name, "ResTexts_" + lang);
        }
    }
}