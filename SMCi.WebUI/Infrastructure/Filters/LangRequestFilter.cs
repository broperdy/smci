﻿using System.Web.Mvc;
using SMCi.WebCore.Extensions;
using Core.Configuration;
using Core.Dependency;

namespace SMCi.WebUI.Infrastructure.Filters
{
    public class LangRequestFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.UrlReferrer != null)
            {
                var routeValues = filterContext.HttpContext.Request.UrlReferrer.AbsoluteUri.Split('/');
                if (routeValues.Length > 3 && !string.IsNullOrEmpty(routeValues[3]))
                {
                    var lang = routeValues[3];
                    if (lang != "en" && lang != "vi")
                    {
                        lang = DependencyManager.Current.GetInstance<ISettingManager>().DefaultLang();
                    }

                    filterContext.RequestContext.RouteData.Values["lang"] = lang;
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}