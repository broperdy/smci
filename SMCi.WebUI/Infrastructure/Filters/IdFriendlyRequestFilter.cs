﻿using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace SMCi.WebUI.Infrastructure.Filters
{
    public class IdFriendlyRequestFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var routeData = filterContext.RequestContext.RouteData;

            if (routeData != null)
            {
                if (routeData.Values.ContainsKey("id"))
                {
                    var stringId = GetIdValue(routeData.Values["id"]).ToString();
                    int routeId;
                    int.TryParse(stringId, out routeId);

                    if (routeId > 0)
                    {
                        // This is important
                        filterContext.ActionParameters["id"] = routeData.Values["id"] = routeId;
                        //routeData.Values["action"] = "Detail";
                    }
                    else
                    {
                        routeData.Values.Remove("id");
                        filterContext.ActionParameters.Remove("id");
                        //routeData.Values["action"] = "Index";
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }

        private object GetIdValue(object id)
        {
            if (id != null)
            {
                string idValue = id.ToString();

                var regex = new Regex(@"^(?<id>\d+).*$");
                var match = regex.Match(idValue);

                if (match.Success)
                {
                    return match.Groups["id"].Value;
                }
            }

            return id;
        }
    }
}