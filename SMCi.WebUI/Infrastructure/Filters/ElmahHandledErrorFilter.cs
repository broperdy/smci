﻿using System.Web.Mvc;
using Elmah;

namespace SMCi.WebUI.Infrastructure.Filters
{
    public class ElmahHandledErrorFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            // Log only handled exceptions, because all other will be caught by ELMAH anyway.
            if (context.ExceptionHandled)
                ErrorSignal.FromCurrentContext().Raise(context.Exception);
        }
    }
}