﻿using System.Web.Mvc;
using SMCi.WebCore.AspNetIdentity;
using Core.Configuration.Startup;
using Core.DataAccess.Uow;

namespace SMCi.WebUI.Infrastructure.Views
{
    public abstract class AppWebViewPage : WebViewPage
    {

    }

    /// <summary>
    /// Base page adding support for the new helpers in all views.
    /// </summary>
    public abstract class AppWebViewPage<TModel> : WebViewPage<TModel>
    {
        public ICurrentUser CurrentUser => DependencyResolver.Current.GetService<ICurrentUser>();
        public ISiteConfig SiteConfig => DependencyResolver.Current.GetService<ISiteConfig>();
        public IUnitOfWork UnitOfWork => DependencyResolver.Current.GetService<IUnitOfWork>();
        public int CurrentUserId => CurrentUser.Id;
        private class View
        {
        }

        public T GetService<T>()
        {
            return DependencyResolver.Current.GetService<T>();
        }
    }
}