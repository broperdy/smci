﻿using System.Linq;
using System.Web.Mvc;

namespace SMCi.WebUI.Infrastructure.Views
{
    public class ExtendedRazorViewEngine : RazorViewEngine
    {
        public ExtendedRazorViewEngine()
        {
            string[] additionalViews = {
                  "~/Views/Shared/AdminPartials/{0}.cshtml",
                  "~/Views/Shared/PartialViews/{0}.cshtml"
                };

            PartialViewLocationFormats = PartialViewLocationFormats.Union(additionalViews).ToArray();
        }
    }
}