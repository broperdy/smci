﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using SMCi.WebCore.Extensions;
using SMCi.WebCore.Modules.Accounts.Models;
using SMCi.WebCore.Modules.Accounts.Services;
using SMCi.WebUI.Controllers.Base;
using Core.Web.Caching;
using Core.Web.Mvc.Filters;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace SMCi.WebUI.Controllers
{
    public class AccountController : AuthorizedController
    {
        private readonly IAuthenticationManager _authenticationManager;
        private readonly ISessionCache _session;
        private readonly IAccountService _service;

        public AccountController(
            IAccountService service,
            IAuthenticationManager authenticationManager,
            ISessionCache session)
        {
            _service = service;
            _authenticationManager = authenticationManager;
            _session = session;
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            var user = await _service.FindUserBy(model.Username, model.Password);

            if (user == null)
            {
                ModelState.AddModelError("", @"Username or Password is incorrect.");
                return View(model);
            }
            if (!user.EmailConfirmed)
            {
                return RedirectToAction("CheckConfirmEmail", "Account", model);
            }

            var result = await _service.Login(model);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    ModelState.AddModelError("", @"This account has been locked out.");
                    return View(model);
                default:
                    ModelState.AddModelError("", @"Invalid login attempt.");
                    return View(model);
            }
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View(new AccountModel());
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Register(AccountModel model)
        {
            try
            {
                var result = await _service.Register(model);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }

                AddErrors(result);
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult CheckConfirmEmail(LoginModel model)
        {
            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> ResendConfirmEmail(string username, string password)
        {
            var user = await _service.FindUserBy(username, password);

            if (user != null)
            {
                await _service.SendConfirmEmail(user.Id, user.Email);
            }
            return RedirectToAction("CheckConfirmEmail", "Account", new LoginModel { Username = username, Password = password });
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(int userId, string code)
        {
            return View((await _service.ConfirmEmail(userId, code)).Succeeded ? "ConfirmedEmail" : "Error");
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            try
            {
                var result = await _service.ForgotPassword(model);
                if (result == null)
                {
                    ModelState.AddModelError("", @"The email does not exist");
                }
                else
                {
                    return View("ForgotPasswordConfirmation");
                }
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(int? userId = null, string code = null)
        {
            if (code == null)
            {
                return RedirectToAction("Index", "Error", new { area = "" });
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordModel model)
        {
            try
            {
                var result = await _service.ResetPassword(model);
                if (result.Succeeded)
                {
                    return RedirectToAction("ResetPasswordConfirmation", "Account");
                }

                AddErrors(result);
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        public ActionResult Logout()
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            _session.Clear();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult TermOfUse()
        {
            return PartialView("Partials/_TermOfUse");
        }

        #region Helpers

        private ActionResult RedirectToLocal(string returnUrl = null)
        {
            if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("PersonalInfo", "Profile", new { Area = "Admin"});
        }

        private void AddErrors(IdentityResult result)
        {
            ModelState.AddIdentityResultErrors(result);
        }

        #endregion
    }
}