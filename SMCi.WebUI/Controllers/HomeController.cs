﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SMCi.DataAccess.Entities;
using SMCi.WebCore.Modules.Home.Services;
using SMCi.WebCore.Modules.Posts.Models;
using SMCi.WebCore.Modules.Posts.Services;
using SMCi.WebUI.Controllers.Base;

namespace SMCi.WebUI.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IHomeService _service;

        public HomeController(IHomeService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View("Company");
        }

        // Company's functions
        public ActionResult Company()
        {
            return View();
        }

        public ActionResult CompanyDetail(int id)
        {
            return View(_service.GetCompany(id));
        }

        [HttpPost]
        public ActionResult GetDistricts()
        {
            return Json(_service.GetDistricts());
        }

        [HttpPost]
        public ActionResult GetTowns(int districtId)
        {
            return Json(_service.GetTowns(districtId));
        }

        [HttpPost]
        public async Task<ActionResult> SearchCompany(string search, int districtId, int townId, int pageIndex = 0)
        {
            return PartialView("_CompanyItem", await _service.SearchCompany(search, districtId, townId, pageIndex));
        }

        //Van ban phap quy's functions
        public ActionResult Document()
        {
            return View();
        }

        public ActionResult DocumentViewer(string fileName)
        {
            return PartialView("_DocViewer", fileName);
        }

        [HttpPost]
        public async Task<ActionResult> SearchDocuments(string search, int catId, int pageIndex = 0)
        {
            return PartialView("_DocumentItem", await _service.SearchDocuments(search, catId, pageIndex));
        }

        public ActionResult GetFileView(string fileName)
        {
            var filePath = fileName;
            Response.AddHeader("Content-Disposition", "inline; filename=" + fileName);

            return File(filePath, MimeMapping.GetMimeMapping(fileName));
        }

        // Thong Tin Phan Anh's function
        public ActionResult Feedback()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<ActionResult> SearchFeedback(string search, int pageIndex = 0)
        {
            return PartialView("_FeedbackItem", await _service.SearchFeedback(search,  pageIndex));
        }

        [HttpPost]
        public async Task<ActionResult> SendFeedback(ThongTinTiepNhan model)
        {
            try
            {
                return Json(await _service.SaveFeedback(model));
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}