using System;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SMCi.WebCore.Extensions;
using Core.Configuration;
using Core.Dependency;
using Core.Web.Mvc.Models;
using Elmah;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SMCi.WebUI.Infrastructure.Filters;

namespace SMCi.WebUI.Controllers.Base
{
    [IdFriendlyRequestFilter]
    public abstract class BaseController : Controller
    {
        protected void HandleError(Exception ex)
        {
            string message = null;
            if (ex.HandleSqlException(ref message))
            {
                ModelState.AddModelError("", message);
            }
            else
            {
                ModelState.AddException(ex);
            }
            ErrorSignal.FromCurrentContext().Raise(ex);
        }

        protected async Task<ActionResult> ExecuteGridActionAsync<T>(DataSourceRequest request, T model, Func<Task> action)
        {
            if (model != null && ModelState.IsValid)
            {
                try
                {
                    await action();
                }
                catch (Exception ex)
                {
                    HandleError(ex);
                }
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult JsonAllowGet(object data)
        {
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public HttpException NotFoundException(string statusDescription = null)
        {
            return new HttpException((int)HttpStatusCode.NotFound, statusDescription);
        }

        public HttpException BadRequestException(string statusDescription = null)
        {
            return new HttpException((int)HttpStatusCode.BadRequest, statusDescription);
        }

        public string GetLang()
        {
            return Request.RequestContext.RouteData.Values["lang"]?.ToString() ?? DependencyManager.Current.GetInstance<ISettingManager>().DefaultLang();
        }
    }
}