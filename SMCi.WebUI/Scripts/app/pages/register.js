/// <reference path="../../typings/tsd.d.ts" />
var registerCtrl = (function () {
    var modalTerm;
    var isModalLoaded;
    return {
        init: init,
        showTerm: showTerm
    };
    ///////////////
    function init() {
        modalTerm = new Modal("Term of use");
        modalTerm.scrollable();
        Validator.default();
    }
    function showTerm() {
        modalTerm.toggle();
        if (isModalLoaded)
            return;
        modalTerm.showLoading();
        $.post("/Account/TermOfUse")
            .done(function (html) {
            modalTerm.setContent(html);
            isModalLoaded = true;
        })
            .fail(function (e) {
            modalTerm.setContent(AjaxHelper.getError(e));
        })
            .always(function () {
            modalTerm.hideLoading();
        });
    }
})();
$(function () {
    registerCtrl.init();
});
