/// <reference path="../../../typings/tsd.d.ts" />
var techblogCtrl = (function () {
    var ajaxBlogtContent;
    return {
        init: init
    };
    function init() {
        ajaxBlogtContent = $("#ajaxBlogtContent");
        loadBlog();
    }
    function loadBlog() {
        $.post("/en/TechBlog/BlogPartial/")
            .done(function (html) {
            ajaxBlogtContent.html(html);
        })
            .fail(function (e) {
            AjaxHelper.fail(e);
        });
    }
})();
$(function () {
    techblogCtrl.init();
});
