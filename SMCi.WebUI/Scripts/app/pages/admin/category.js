/// <reference path="../../../typings/tsd.d.ts" />
var categoryCtrl = (function () {
    var treeList;
    return {
        deleteNode: deleteNode,
        init: init
    };
    ///////////////
    function init() {
        treeList = $("#treeList").data("kendoTreeList");
    }
    function deleteNode(e) {
        if (!confirm("Are you sure you want to delete this record?"))
            return;
        var row = $(e.target).closest("tr");
        var model = treeList.dataItem(row);
        $.post("/Category/Delete", { id: model.id })
            .done(function (data) {
            if (AjaxHelper.handleError(data))
                return;
            row.remove();
            Notify.success("Delete category successfully");
            if (model.parentId != null) {
                treeList.dataSource.read();
            }
        })
            .fail(AjaxHelper.fail);
    }
})();
$(function () {
    categoryCtrl.init();
});
