/// <reference path="../../../typings/tsd.d.ts" />
var hostManageUserCtrl = (function () {
    var grid;
    return {
        init: init,
        toggleIsLocked: toggleIsLocked,
        resetPassword: resetPassword
    };
    ///////////////
    function init() {
        grid = $("#grid").data("kendoGrid");
    }
    function toggleIsLocked(e) {
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (!confirm("Are you sure you want to " + (dataItem.IsLocked ? 'unlock' : 'lock') + " this user?"))
            return;
        $.post("/Admin/ManageUser/ToggleIsLocked/", { id: dataItem.Id })
            .done(function (data) {
            if (AjaxHelper.handleError(data))
                return;
            dataItem.set("IsLocked", !dataItem.IsLocked);
            Notify.success((dataItem.IsLocked ? 'Lock' : 'Unlock') + " successfully");
        })
            .fail(AjaxHelper.fail);
    }
    function resetPassword(id) {
        if (!confirm("Are you sure you want to reset password for this user?"))
            return;
        $.post("/Admin/ManageUser/ResetPassword/", { id: id })
            .done(function (data) {
            if (AjaxHelper.handleError(data))
                return;
            Notify.success("Password reset successfully");
        })
            .fail(AjaxHelper.fail);
    }
})();
$(function () {
    hostManageUserCtrl.init();
});
