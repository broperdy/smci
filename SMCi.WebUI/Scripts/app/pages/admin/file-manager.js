/// <reference path="../../../typings/tsd.d.ts" />
var fileCtrl = (function () {
    var targetId;
    var btDelete;
    var listView;
    return {
        init: init,
        chooseFile: chooseFile,
        onChange: onChange,
        onDataBound: onDataBound
    };
    ///////////////
    function init(id) {
        Validator.default();
        targetId = id;
        listView = $("#listView").data("kendoListView");
        btDelete = $("#btDelete");
        btDelete.click(deleteImage);
    }
    function chooseFile(path) {
        $("#" + targetId, opener.document).val(path);
        window.close();
    }
    function onChange(e) {
        btDelete.show();
    }
    function onDataBound(e) {
        btDelete.hide();
    }
    function deleteImage(e) {
        if (!confirm("Are you sure you want to delete selected image?"))
            return;
        var index = listView.select().index(), dataSource = listView.dataSource, dataItem = dataSource.view()[index];
        $.post("/FileManager/DeleteFile", { name: dataItem.Name, path: dataItem.Path })
            .done(function (data) {
            if (AjaxHelper.handleError(data))
                return;
            Notify.success("Delete file successfully");
            dataSource.remove(dataItem);
        })
            .fail(AjaxHelper.fail);
    }
})();
