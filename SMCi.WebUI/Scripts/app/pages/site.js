var winDow = $(window);
$(function () {
    /* ---------------------------------------------------------------------- */
    /*	menu responsive
    /* ---------------------------------------------------------------------- */
    var menuClick = $('a.elemadded'), navbarVertical = $('.menu-box');
    menuClick.on('click', function (e) {
        e.preventDefault();
        if (navbarVertical.hasClass('active')) {
            navbarVertical.slideUp(300).removeClass('active');
        }
        else {
            navbarVertical.slideDown(300).addClass('active');
        }
    });
    winDow.bind('resize', function () {
        if (winDow.width() > 768) {
            navbarVertical.slideDown(300).removeClass('active');
        }
        else {
            navbarVertical.slideUp(300).removeClass('active');
        }
    });
});
