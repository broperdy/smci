/// <reference path="../../../typings/tsd.d.ts" />
var documentCtrl = (function () {
    var ajaxCategoryContent;
    var ajaxDocContent;
    var loadingContent;
    var currentPaging = 0;
    var isEnding = false;
    return {
        init: init,
        search: search
    };
    function init() {
        ajaxCategoryContent = $("#ajax-category-content");
        ajaxDocContent = $("#ajax-document-content");
        loadingContent = $("#loading-content");
        search();
        //Load more image when scroll
        $(window).scroll(function () {
            if (!isEnding) {
                if ($(window).scrollTop() === $(document).height() - $(window).height()) {
                    if (!isEnding) {
                        currentPaging++;
                        search("", currentPaging);
                    }
                }
            }
        });
        var input = document.getElementById("search-input");
        input.addEventListener("keyup", function (event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("search-button").click();
            }
        });
    }
    function loadCategories() {
        $.post("/Home/GetDocCategories/")
            .done(function (data) {
            ajaxCategoryContent.html("");
            data.forEach(function (item, i) {
                ajaxCategoryContent.append("<li>\n                        <span class=\"badge badge-u\">12234</span>\n                        <a href=\"/tinh-thanh-pho/an-giang\">C\u00F4ng v\u0103n</a>\n                    </li>");
            });
        })
            .fail(function (e) {
            AjaxHelper.fail(e);
        });
    }
    function search(catId, paging) {
        if (catId === void 0) { catId = -1; }
        if (paging === void 0) { paging = 0; }
        // Handle paging
        if (paging === 0) {
            currentPaging = 0;
            isEnding = false;
        }
        else {
            currentPaging = paging;
        }
        loadingContent.html("<div class=\"sk-spinner sk-spinner-three-bounce\"><div class=\"sk-bounce1\"></div><div class=\"sk-bounce2\"></div><div class=\"sk-bounce3\"></div></div>");
        $.post("/Home/SearchDocuments/", { search: $("#search-input").val(), catId: catId, pageIndex: currentPaging })
            .done(function (data) {
            if (!data)
                isEnding = true;
            loadingContent.html("");
            if (currentPaging === 0) {
                ajaxDocContent.html("");
            }
            ajaxDocContent.append(data);
        })
            .fail(function (e) {
            AjaxHelper.fail(e);
        }).always(function () {
            Ladda.stopAll();
        });
    }
})();
$(function () {
    documentCtrl.init();
});
