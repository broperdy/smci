/// <reference path="../../../typings/tsd.d.ts" />
var Ladda;
var companyCtrl = (function () {
    var ajaxDistrictContent;
    var ajaxDistrictMenu;
    var ajaxTownContent;
    var ajaxCompanyContent;
    var loadingContent;
    var currentPaging = 0;
    var isEnding = false;
    return {
        init: init,
        search: search
    };
    function init() {
        ajaxDistrictContent = $("#ajax-district-content");
        ajaxDistrictMenu = $("#menu-district-content");
        ajaxTownContent = $("#ajax-town-content");
        ajaxCompanyContent = $("#ajax-company-content");
        loadingContent = $("#loading-content");
        loadDistricts();
        search();
        ajaxDistrictContent.change(function () {
            ajaxTownContent.html("<div class=\"sk-spinner sk-spinner-three-bounce\"><div class=\"sk-bounce1\"></div><div class=\"sk-bounce2\"></div><div class=\"sk-bounce3\"></div></div>");
            loadTowns(ajaxDistrictContent.val());
        });
        //Load more image when scroll
        $(window).scroll(function () {
            if (!isEnding) {
                if ($(window).scrollTop() === $(document).height() - $(window).height()) {
                    if (!isEnding) {
                        currentPaging++;
                        search("", currentPaging);
                    }
                }
            }
        });
        var input = document.getElementById("search-input");
        input.addEventListener("keyup", function (event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("search-button").click();
            }
        });
    }
    function loadDistricts() {
        $.post("/Home/GetDistricts/")
            .done(function (data) {
            ajaxDistrictMenu.html("");
            data.forEach(function (item, i) {
                ajaxDistrictContent.append("<option value=\"" + item.DistrictID + "\" >" + item.DistrictName + "</option>");
                ajaxDistrictMenu.append("<li>\n                        <span class=\"badge badge-u\">" + item.CompanyCount + "</span>\n                        <a href=\"javascript:companyCtrl.search('" + item.DistrictID + "')\">" + item.DistrictName + "</a>\n                    </li>");
            });
        })
            .fail(function (e) {
            AjaxHelper.fail(e);
        });
    }
    function loadTowns(districtId) {
        $.post("/Home/GetTowns/", { districtId: districtId })
            .done(function (data) {
            var htmlTownContent = "<select id=\"select-town-option\" class=\"form-control\"><option value=\"-1\" selected=\"\">Ph\u01B0\u1EDDng/X\u00E3</option>";
            data.forEach(function (item, i) {
                htmlTownContent += "<option value=\"" + item.TownID + "\">" + item.TownName + "</option>";
            });
            ajaxTownContent.html(htmlTownContent + "</select>");
        })
            .fail(function (e) {
            AjaxHelper.fail(e);
        });
    }
    function search(districtId, paging) {
        if (districtId === void 0) { districtId = ""; }
        if (paging === void 0) { paging = 0; }
        var districtIdSearch = ajaxDistrictContent.val();
        if (districtId) {
            districtIdSearch = districtId;
            ajaxDistrictContent.val(districtId);
        }
        // Handle paging
        if (paging === 0) {
            currentPaging = 0;
            isEnding = false;
        }
        else {
            currentPaging = paging;
        }
        loadingContent.html("<div class=\"sk-spinner sk-spinner-three-bounce\"><div class=\"sk-bounce1\"></div><div class=\"sk-bounce2\"></div><div class=\"sk-bounce3\"></div></div>");
        if (currentPaging === 0) {
            ajaxCompanyContent.html("");
        }
        $.post("/Home/SearchCompany/", { search: $("#search-input").val(), districtId: districtIdSearch, townId: $("#select-town-option").val(), pageIndex: currentPaging })
            .done(function (data) {
            if (!data)
                isEnding = true;
            loadingContent.html("");
            ajaxCompanyContent.append(data);
        })
            .fail(function (e) {
            AjaxHelper.fail(e);
        })
            .always(function () {
            Ladda.stopAll();
        });
    }
})();
$(function () {
    companyCtrl.init();
});
