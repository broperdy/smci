// Preloader
$(window).on('load', function () {
    $('.preloader-wave-effect').fadeOut();
    $('#preloader-wrapper').delay(100).fadeOut('slow');
});
