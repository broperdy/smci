/// <reference path="../../../typings/tsd.d.ts" />
var feedbackCtrl = (function () {
    var ajaxFeedbackContent;
    var loadingContent;
    var currentPaging = 0;
    var isEnding = false;
    return {
        init: init,
        search: search,
        sendFeedback: sendFeedback,
        viewAnswer: viewAnswer
    };
    function init() {
        ajaxFeedbackContent = $("#ajax-feedback-content");
        loadingContent = $("#loading-content");
        search();
        $("#feedback-content").on("scroll", function () {
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                if (!isEnding) {
                    currentPaging++;
                    search(currentPaging);
                }
            }
        });
        var input = document.getElementById("search-input");
        input.addEventListener("keyup", function (event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("search-button").click();
            }
        });
        $("#form").submit(function (event) {
            event.preventDefault();
            sendFeedback();
        });
    }
    function search(paging) {
        if (paging === void 0) { paging = 0; }
        // Handle paging
        if (paging === 0) {
            currentPaging = 0;
            isEnding = false;
        }
        else {
            currentPaging = paging;
        }
        loadingContent.html("<div class=\"sk-spinner sk-spinner-three-bounce\"><div class=\"sk-bounce1\"></div><div class=\"sk-bounce2\"></div><div class=\"sk-bounce3\"></div></div>");
        $.post("/Home/SearchFeedback/", { search: $("#search-input").val(), pageIndex: currentPaging })
            .done(function (data) {
            if (!data)
                isEnding = true;
            loadingContent.html("");
            if (currentPaging === 0) {
                ajaxFeedbackContent.html("<tr>\n                                                <th>M\u00E3 ti\u1EBFp nh\u1EADn</th>\n                                                <th>Ti\u00EAu \u0111\u1EC1</th>\n                                                <th>N\u1ED9i dung</th>\n                                                <th>Tr\u1EA3 l\u1EDDi</th>\n                                            </tr>");
            }
            ajaxFeedbackContent.append(data);
        })
            .fail(function (e) {
            AjaxHelper.fail(e);
        }).always(function () {
            Ladda.stopAll();
        });
    }
    function sendFeedback() {
        $.post("/Home/SendFeedback/", {
            HoTen: $("#form-hoten").val(),
            Email: $("#form-email").val(),
            TieuDe: $("#form-tieude").val(),
            NoiDung: $("#form-noidung").val()
        })
            .done(function (data) {
            if (!data) {
                Notify.error("Thông tin tiếp nhận gửi không thành công");
            }
            else {
                Notify.success("Thông tin tiếp nhận đã được gửi thành công. Mã tiếp nhận : " + data);
                $("#form-hoten").val("");
                $("#form-email").val("");
                $("#form-tieude").val("");
                $("#form-noidung").val("");
            }
        })
            .fail(function (e) {
            Notify.error("Thông tin tiếp nhận gửi không thành công");
            AjaxHelper.fail(e);
        }).always(function () {
            Ladda.stopAll();
        });
    }
    function viewAnswer(title, tieude, noidung, traloi) {
        $("#pop-title").html(title);
        $("#pop-tieude").html(tieude);
        $("#pop-noidung").html(noidung);
        $("#pop-traloi").html(traloi);
    }
})();
$(function () {
    feedbackCtrl.init();
});
