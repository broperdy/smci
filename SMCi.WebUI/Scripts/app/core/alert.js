var swalColors = {
    primary: "#1A7BB9",
    success: "#1c84c6",
    info: "#23c6c8",
    warning: "#f8ac59",
    danger: "#ed5565"
};
var Alert = /** @class */ (function () {
    function Alert() {
    }
    Alert.success = function (title, text) {
        swal({
            title: title,
            text: text,
            type: "success",
            confirmButtonColor: swalColors.primary
        });
    };
    Alert.error = function (title, text) {
        swal({
            title: title,
            text: text,
            type: "error",
            confirmButtonColor: swalColors.primary
        });
    };
    Alert.info = function (title, text) {
        swal({
            title: title,
            text: text,
            type: "info",
            confirmButtonColor: swalColors.info
        });
    };
    Alert.warning = function (title, text) {
        swal({
            title: title,
            text: text,
            type: "warning",
            confirmButtonColor: swalColors.primary
        });
    };
    Alert.confirm = function (title, text) {
        return swal({
            title: title,
            text: text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: swalColors.danger
        });
    };
    return Alert;
}());
