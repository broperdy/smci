var Modal = /** @class */ (function () {
    function Modal(title, selector) {
        if (selector === void 0) { selector = "#modal"; }
        this.selector = selector;
        this.modal = selector instanceof jQuery ? selector : $(this.selector);
        this.modalTitle = this.modal.find(".modal-title");
        this.modalBody = this.modal.find(".modal-body");
        this.modalContent = this.modalBody.find("#ajaxContent");
        this.spinner = this.modal.find(".modal-body .sk-spinner");
        this.setTitle(title);
        this.hideLoading();
    }
    Modal.prototype.setLoading = function (value) {
        if (value) {
            this.setContent("");
            this.spinner.show();
        }
        else {
            this.spinner.hide();
        }
    };
    Modal.prototype.showLoading = function () {
        this.setLoading(true);
    };
    Modal.prototype.hideLoading = function () {
        this.setLoading(false);
    };
    Modal.prototype.scrollable = function (enabled) {
        if (enabled === void 0) { enabled = true; }
        if (enabled) {
            this.modalBody.addClass("modal-scroll");
        }
        else {
            this.modalBody.removeClass("modal-scroll");
        }
    };
    Modal.prototype.setTitle = function (value) {
        this.modalTitle.html(value);
    };
    Modal.prototype.setContent = function (value) {
        this.modalContent.html(value);
    };
    Modal.prototype.toggle = function () {
        this.modal.modal("toggle");
    };
    Modal.prototype.findInContent = function (selector) {
        return this.modalContent.find(selector);
    };
    return Modal;
}());
