/// <reference path="../../typings/tsd.d.ts" />
var Validator = /** @class */ (function () {
    function Validator(selector, rules) {
        if (selector === void 0) { selector = "#form"; }
        this.selector = selector;
        this.defaultRules = {
            compare: function (input) {
                if (input.is("[data-compare-msg]") && input.data("compare")) {
                    return input.val() === $(input.data("compare")).val();
                }
                return true;
            },
            mask: function (input) {
                if (input.is("[data-mask-msg]") && input.val() !== "") {
                    var maskedtextbox = input.data("kendoMaskedTextBox");
                    return maskedtextbox.value().indexOf(maskedtextbox.options.promptChar) === -1;
                }
                return true;
            },
            pattern: function (input) {
                if (input.is("[data-pattern-msg]") && input.val() !== "") {
                    var reg = new RegExp(input.data("pattern"));
                    return reg.test(input.val());
                }
                return true;
            },
            minlength: function (input) {
                if (input.is("[data-minlength-msg]")) {
                    var minlength = input.data("minlength");
                    return input.val().length === 0 || input.val().length >= minlength;
                }
                return true;
            },
            depRequired: function (input) {
                if (input.is("[data-deprequired-msg]") && input.data("otherprop")) {
                    var depValue = $(input.data("otherprop")).val();
                    return (input.val() !== "" && depValue !== "") || (depValue === "" && input.val() === "") || input.val();
                }
                return true;
            },
            depRequiredIf: function (input) {
                if (input.is("[data-deprequiredif-msg]") && input.data("otherprop")) {
                    var depValue = $(input.data("otherprop")).val();
                    return (input.val() !== "" && depValue === true) || (depValue === false && input.val() === "") || input.val();
                }
                return true;
            },
            selfRef: function (input) {
                if (input.is("[data-selfref-msg]") && input.data("ref")) {
                    return $(input.data("ref")).val() !== input.val();
                }
                return true;
            },
            upload: function (input) {
                var fileInput = input[0];
                if (fileInput.type === "file") {
                    var $file = $(fileInput);
                    var uploadedFiles = input.closest(".k-upload").find(".k-file");
                    if ($file.data("upload") === "required" && uploadedFiles.length === 0) {
                        return false;
                    }
                    return !uploadedFiles.hasClass("k-file-invalid");
                }
                return true;
            },
            boolRequired: function (input) {
                if (input.is("[data-boolrequired-msg]") && input.data("boolean")) {
                    return input.prop("checked");
                }
                return true;
            }
        };
        this.form = selector instanceof jQuery ? selector : $(this.selector);
        this.registerEventIfAnyUpload();
        this.validatable = this.form.kendoValidator({
            rules: $.extend(true, this.defaultRules, rules)
        }).data("kendoValidator");
    }
    Object.defineProperty(Validator.prototype, "isValid", {
        get: function () {
            return this.validatable.validate();
        },
        enumerable: true,
        configurable: true
    });
    // HACK: http://www.telerik.com/forums/validating-upload
    Validator.prototype.registerEventIfAnyUpload = function () {
        var _this = this;
        if (this.form.find(".k-upload input[type=file]").length === 0)
            return;
        this.form.submit(function (e) {
            // MUST find all again (DO NOT use cached variable here)
            _this.form.find(".k-upload input[type=file]").removeAttr("disabled");
            if (!_this.isValid) {
                e.preventDefault();
            }
        });
    };
    Validator.for = function (selector, rules) {
        return new Validator(selector, rules);
    };
    Validator.default = function (rules) {
        return new Validator("form", rules);
    };
    return Validator;
}());
