﻿/// <reference path="../../../typings/tsd.d.ts" />
var activationCtrl = (() => {
    var detailsTemplate: any;

    return {
        init: init,
        showDetails: showDetails,
        checkNull: checkNull
    };

    ///////////////
    function init() {
        detailsTemplate = kendo.template($("#template").html());
    }

    function showDetails(e: any) {
        e.preventDefault();

        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        var wnd = $("#Details").data("kendoWindow");

        wnd.content(detailsTemplate(dataItem));
        wnd.center().open();
    }

    function checkNull(item) {
        return item === null ? "" : item;
    }
})();


$(() => {
    activationCtrl.init();
});