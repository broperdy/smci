﻿/// <reference path="../../../typings/tsd.d.ts" />
var homeCtrl = (() => {
    var ajaxPosttContent: any;

    return {
        init: init
    };

    function init() {
        ajaxPosttContent = $("#ajax-post-content");

        loadPosts();
    }

    function loadPosts() {
        $.post("/Home/GetPost/")
            .done(data => {
                ajaxPosttContent.html("");
                data.forEach((item, i) => {
                    ajaxPosttContent.append(`<div class="col-md-12" style='padding:0'>
                        <div class="ibox" style='margin-bottom:10px!important'>
                            <div class="ibox-content product-box">
                                <div class="product-desc row">
                                    <div class='col-md-3'>
                                        <img src='${item.Image}' style='height:200px'></img>
                                    </div>
                                    <div class='col-md-9'>
                                        <a href="#" class="product-name"> ${item.Title}</a>
                                        <div class="small m-t-xs">
                                            ${item.Content}
                                        </div>
                                        <div class="m-t text-righ">
                                            <a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`);
                });
            })
            .fail(e => {
                AjaxHelper.fail(e);
            });
    }
})();

$(() => {
    homeCtrl.init();
});