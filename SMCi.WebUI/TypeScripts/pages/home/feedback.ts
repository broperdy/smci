﻿/// <reference path="../../../typings/tsd.d.ts" />
var feedbackCtrl = (() => {
    var ajaxFeedbackContent: any;
    var loadingContent: any;

    var currentPaging = 0;
    var isEnding = false;

    return {
        init: init,
        search: search,
        sendFeedback: sendFeedback,
        viewAnswer: viewAnswer
    };

    function init() {
        ajaxFeedbackContent = $("#ajax-feedback-content");
        loadingContent = $("#loading-content");

        search();

        $("#feedback-content").on("scroll", function () {
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                if (!isEnding) {
                    currentPaging++;
                    search(currentPaging);
                }
            }
        });

        const input = document.getElementById("search-input");
        input.addEventListener("keyup", event => {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("search-button").click();
            }
        });

        $("#form").submit(function (event) {
            event.preventDefault();
            sendFeedback();
        });
    }

    function search(paging: number = 0) {
        // Handle paging
        if (paging === 0) {
            currentPaging = 0;
            isEnding = false;
        } else {
            currentPaging = paging;
        }

        loadingContent.html(`<div class="sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div>`);
        $.post("/Home/SearchFeedback/", { search: $("#search-input").val(), pageIndex: currentPaging })
            .done(data => {
                if (!data) isEnding = true;
                loadingContent.html("");
                if (currentPaging === 0) {
                    ajaxFeedbackContent.html(`<tr>
                                                <th>Mã tiếp nhận</th>
                                                <th>Tiêu đề</th>
                                                <th>Nội dung</th>
                                                <th>Trả lời</th>
                                            </tr>`);
                }
                ajaxFeedbackContent.append(data);
            })
            .fail(e => {
                AjaxHelper.fail(e);
            }).always(() => {
                Ladda.stopAll();
            });
    }

    function sendFeedback() {
        $.post("/Home/SendFeedback/",
            {
                HoTen: $("#form-hoten").val(),
                Email: $("#form-email").val(),
                TieuDe: $("#form-tieude").val(),
                NoiDung: $("#form-noidung").val()
            })
            .done(data => {
                if (!data) {
                    Notify.error("Thông tin tiếp nhận gửi không thành công");
                } else {
                    Notify.success("Thông tin tiếp nhận đã được gửi thành công. Mã tiếp nhận : " + data);
                    $("#form-hoten").val("");
                    $("#form-email").val("");
                    $("#form-tieude").val("");
                    $("#form-noidung").val("");
                }
            })
            .fail(e => {
                Notify.error("Thông tin tiếp nhận gửi không thành công");
                AjaxHelper.fail(e);
            }).always(() => {
                Ladda.stopAll();
            });
    }

    function viewAnswer(title, tieude, noidung, traloi) {
        $("#pop-title").html(title);
        $("#pop-tieude").html(tieude);
        $("#pop-noidung").html(noidung);
        $("#pop-traloi").html(traloi);
    }
})();

$(() => {
    feedbackCtrl.init();
});