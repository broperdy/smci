﻿/// <reference path="../../../typings/tsd.d.ts" />
var documentCtrl = (() => {
    var ajaxCategoryContent: any;
    var ajaxDocContent: any;
    var loadingContent: any;

    var currentPaging = 0;
    var isEnding = false;

    return {
        init: init,
        search: search
    };

    function init() {
        ajaxCategoryContent = $("#ajax-category-content");
        ajaxDocContent = $("#ajax-document-content");
        loadingContent = $("#loading-content");

        search();

        //Load more image when scroll
        $(window).scroll(() => {
            if (!isEnding) {
                if ($(window).scrollTop() === $(document).height() - $(window).height()) {
                    if (!isEnding) {
                        currentPaging++;
                        search("", currentPaging);
                    }
                }
            }
        });

        const input = document.getElementById("search-input");
        input.addEventListener("keyup", event => {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("search-button").click();
            }
        });
    }

    function loadCategories() {
        $.post("/Home/GetDocCategories/")
            .done(data => {
                ajaxCategoryContent.html("");
                data.forEach((item, i) => {
                    ajaxCategoryContent.append(`<li>
                        <span class="badge badge-u">12234</span>
                        <a href="/tinh-thanh-pho/an-giang">Công văn</a>
                    </li>`);
                });
            })
            .fail(e => {
                AjaxHelper.fail(e);
            });
    }

    function search(catId: any = -1, paging: number = 0) {
        // Handle paging
        if (paging === 0) {
            currentPaging = 0;
            isEnding = false;
        } else {
            currentPaging = paging;
        }

        loadingContent.html(`<div class="sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div>`);
        $.post("/Home/SearchDocuments/", { search: $("#search-input").val(), catId: catId, pageIndex: currentPaging })
            .done(data => {
                if (!data) isEnding = true;
                loadingContent.html("");
                if (currentPaging === 0) { ajaxDocContent.html(""); }
                ajaxDocContent.append(data);
            })
            .fail(e => {
                AjaxHelper.fail(e);
            }).always(() => {
                Ladda.stopAll();
            });
    }
})();

$(() => {
    documentCtrl.init();
});