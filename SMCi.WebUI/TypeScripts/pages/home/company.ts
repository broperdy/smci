﻿/// <reference path="../../../typings/tsd.d.ts" />
var Ladda;
var companyCtrl = (() => {
    var ajaxDistrictContent: any;
    var ajaxDistrictMenu: any;
    var ajaxTownContent: any;
    var ajaxCompanyContent: any;
    var loadingContent: any;

    var currentPaging = 0;
    var isEnding = false;

    return {
        init: init,
        search: search
    };

    function init() {
        ajaxDistrictContent = $("#ajax-district-content");
        ajaxDistrictMenu = $("#menu-district-content");
        ajaxTownContent = $("#ajax-town-content");
        ajaxCompanyContent = $("#ajax-company-content");
        loadingContent = $("#loading-content");

        loadDistricts();
        search();

        ajaxDistrictContent.change(() => {
            ajaxTownContent.html(`<div class="sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div>`);
            loadTowns(ajaxDistrictContent.val());
        });

        //Load more image when scroll
        $(window).scroll(() => {
            if (!isEnding) {
                if ($(window).scrollTop() === $(document).height() - $(window).height()) {
                    if (!isEnding) {
                        currentPaging++;
                        search("", currentPaging);
                    }
                }
            }
        });

        const input = document.getElementById("search-input");
        input.addEventListener("keyup", event => {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("search-button").click();
            }
        });
    }

    function loadDistricts() {
        $.post("/Home/GetDistricts/")
            .done(data => {
                ajaxDistrictMenu.html("");
                data.forEach((item, i) => {
                    ajaxDistrictContent.append(`<option value="${item.DistrictID}" >${item.DistrictName}</option>`);
                    ajaxDistrictMenu.append(`<li>
                        <span class="badge badge-u">${item.CompanyCount}</span>
                        <a href="javascript:companyCtrl.search('${item.DistrictID}')">${item.DistrictName}</a>
                    </li>`);
                });
            })
            .fail(e => {
                AjaxHelper.fail(e);
            });
    }

    function loadTowns(districtId) {
        $.post("/Home/GetTowns/", { districtId: districtId })
            .done(data => {
                var htmlTownContent = `<select id="select-town-option" class="form-control"><option value="-1" selected="">Phường/Xã</option>`;
                data.forEach((item, i) => {
                    htmlTownContent += `<option value="${item.TownID}">${item.TownName}</option>`;
                });
                ajaxTownContent.html(htmlTownContent + `</select>`);
            })
            .fail(e => {
                AjaxHelper.fail(e);
            });
    }

    function search(districtId: any = "", paging: number = 0) {
        let districtIdSearch = ajaxDistrictContent.val();
        if (districtId) {
            districtIdSearch = districtId;
            ajaxDistrictContent.val(districtId);
        }

        // Handle paging
        if (paging === 0) {
            currentPaging = 0;
            isEnding = false;
        } else {
            currentPaging = paging;
        }

        loadingContent.html(`<div class="sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div>`);
        if (currentPaging === 0) { ajaxCompanyContent.html(""); }
        $.post("/Home/SearchCompany/", { search: $("#search-input").val(), districtId: districtIdSearch, townId: $("#select-town-option").val(), pageIndex: currentPaging })
            .done(data => {
                if (!data) isEnding = true;
                loadingContent.html("");
                ajaxCompanyContent.append(data);
            })
            .fail(e => {
                AjaxHelper.fail(e);
            })
            .always(() => {
                Ladda.stopAll();
            });
    }
})();

$(() => {
    companyCtrl.init();
});