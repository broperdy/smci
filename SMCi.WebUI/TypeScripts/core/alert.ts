﻿declare var swal: any;
var swalColors: any = {
    primary: "#1A7BB9",
    success: "#1c84c6",
    info: "#23c6c8",
    warning: "#f8ac59",
    danger: "#ed5565"
};

class Alert {

    static success(title: string, text: string): void {
        swal({
            title: title,
            text: text,
            type: "success",
            confirmButtonColor: swalColors.primary
        });
    }

    static error(title: string, text: string): void {
        swal({
            title: title,
            text: text,
            type: "error",
            confirmButtonColor: swalColors.primary
        });
    }

    static info(title: string, text: string): void {
        swal({
            title: title,
            text: text,
            type: "info",
            confirmButtonColor: swalColors.info
        });
    }

    static warning(title: string, text: string): void {
        swal({
            title: title,
            text: text,
            type: "warning",
            confirmButtonColor: swalColors.primary
        });
    }

    static confirm(title: string, text: string): any {
        return swal({
            title: title,
            text: text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: swalColors.danger
        });
    }
}
