﻿class Modal {
    private modalContent: JQuery;
    private modal: any;
    private modalTitle: JQuery;
    private modalBody: any;
    private spinner: JQuery;

    constructor(title: string, public selector: any = "#modal") {
        this.modal = selector instanceof jQuery ? selector : $(this.selector);

        this.modalTitle = this.modal.find(".modal-title");
        this.modalBody = this.modal.find(".modal-body");
        this.modalContent = this.modalBody.find("#ajaxContent");
        this.spinner = this.modal.find(".modal-body .sk-spinner");

        this.setTitle(title);
        this.hideLoading();
    }

    setLoading(value: boolean) {
        if (value) {
            this.setContent("");
            this.spinner.show();
        } else {
            this.spinner.hide();
        }
    }

    showLoading() {
        this.setLoading(true);
    }

    hideLoading() {
        this.setLoading(false);
    }

    scrollable(enabled: boolean = true) {
        if (enabled) {
            this.modalBody.addClass("modal-scroll");
        } else {
            this.modalBody.removeClass("modal-scroll");
        }
    }

    setTitle(value: string) {
        this.modalTitle.html(value);
    }

    setContent(value: string) {
        this.modalContent.html(value);
    }

    toggle() {
        this.modal.modal("toggle");
    }

    findInContent(selector: string): JQuery {
        return this.modalContent.find(selector);
    }
}