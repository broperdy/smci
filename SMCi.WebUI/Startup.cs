﻿using SMCi.WebUI;
using Core.Dependency.Tasks;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace SMCi.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            PackageConfig.Register();

            DependencyConfig.Register(app);
            AuthConfig.Register(app);

            ScopeTaskCommand.ExecuteInitTasks();
        }
    }
}
