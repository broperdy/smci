﻿using System.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using SMCi.WebUI.Infrastructure.Views;
using MvcSiteMapProvider.Web.Mvc;

namespace SMCi.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            XmlSiteMapController.RegisterRoutes(routes);

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "SiteMap",
                url: "sitemap.xml",
                defaults: new { controller = "XmlSiteMapController", action = "Index", page = 0 }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "SMCi.WebUI.Controllers" }
            );
        }
    }
}
