﻿using System.Web.Mvc;
using SMCi.WebUI.Infrastructure.Filters;
using Core.Web.Mvc.Filters;

namespace SMCi.WebUI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ElmahHandledErrorFilter());
            filters.Add(new AjaxHandledErrorFilter());
            //filters.Add(new UserAuditAttribute());
        }
    }
}
