﻿using System.Web.Optimization;

namespace SMCi.WebUI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js-admin").Include(
                "~/Scripts/libs/jquery.min.js",
                 "~/Scripts/kendo/kendo.all.min.js",
                 "~/Scripts/kendo/kendo.aspnetmvc.min.js",
                 "~/Scripts/libs/bootstrap.min.js",
                 "~/Scripts/libs/toastr.min.js",
                 "~/Scripts/libs/spin.min.js",
                 "~/Scripts/ladda.js",
                 "~/Scripts/libs/ladda.jquery.min.js",
                 "~/Scripts/plugins/metisMenu/metisMenu.min.js",
                 "~/Scripts/plugins/slimscroll/jquery.slimscroll.min.js",
                 "~/Scripts/plugins/pace/pace.min.js")
                 .IncludeDirectory("~/Scripts/app/core", "*.js")
                 .Include("~/Scripts/app/app.js")
             );

            bundles.Add(new StyleBundle("~/Content/css/admin").Include(
                "~/Content/vendors/bootstrap.css",
                "~/Content/vendors/awesome-bootstrap-checkbox.css",
                "~/Content/vendors/font-awesome.css",
                "~/Content/vendors/toastr.css",
                //"~/Content/vendors/sweetalert2.css",
                "~/Content/vendors/ladda-themeless.min.css",
                "~/Content/css/gallery/blueimp-gallery.min.css",
                "~/Content/css/animate.css",
                "~/Content/css/kendo.common-bootstrap.min.css",
                "~/Content/css/kendo.bootstrap.min.css",
                "~/Content/css/inspinia.css",
                "~/Content/css/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/js-blank").Include(
                "~/Scripts/libs/jquery.min.js",
                "~/Scripts/libs/bootstrap.min.js",
                "~/Scripts/kendo/kendo.all.min.js",
                "~/Scripts/kendo/kendo.aspnetmvc.min.js",
                "~/Scripts/libs/toastr.min.js",
                "~/Scripts/libs/spin.min.js",
                "~/Scripts/ladda.js",
                "~/Scripts/libs/ladda.jquery.min.js")
                .IncludeDirectory("~/Scripts/app/core", "*.js")
                .Include("~/Scripts/app/app_blank.js"));

            bundles.Add(new StyleBundle("~/Content/css/home").Include(
                "~/Content/vendors/bootstrap.css",
                "~/Content/vendors/font-awesome.css",
                "~/Content/vendors/toastr.css",
                "~/Content/vendors/sweetalert2.css",
                "~/Content/vendors/bootstrap-social.css",
                "~/Content/vendors/magnific-popup.css",
                "~/Content/vendors/ladda-themeless.min.css",
                "~/Content/vendors/swiper.css",
                "~/Content/css/inspinia.css",
                "~/Content/css/site.css",
                "~/Content/css/kendo.common-bootstrap.min.css",
                "~/Content/css/kendo.material.min.css",
                "~/Content/home/css/cubeportfolio.css",
                "~/Content/home/css/customize.css",
                "~/Content/css/jquery.bootstrap-touchspin.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/js-home").Include(
                "~/Scripts/libs/jquery.min.js",
                 "~/Scripts/libs/bootstrap.min.js",
                 "~/Scripts/libs/toastr.min.js",
                 "~/Scripts/libs/spin.min.js",
                 "~/Scripts/ladda.js",
                 "~/Scripts/libs/ladda.jquery.min.js",
                 "~/Scripts/plugins/metisMenu/metisMenu.min.js",
                 "~/Scripts/plugins/slimscroll/jquery.slimscroll.min.js",
                 "~/Scripts/plugins/wow/wow.min.js",
                 "~/Scripts/plugins/pace/pace.min.js",
                 "~/Scripts/home/front-end/jquery.cubeportfolio.js",
                "~/Scripts/kendo/kendo.all.min.js",
                "~/Scripts/kendo/kendo.aspnetmvc.min.js",
                 "~/Scripts/app/pages/site.js")
                 .IncludeDirectory("~/Scripts/app/core", "*.js")
                 .Include("~/Scripts/app/app.js")
            );

            bundles.Add(new StyleBundle("~/Content/home/css/theme").Include(
                "~/Content/home/css/bootstrap.min.css",
                "~/Content/home/css/animated.css",
                "~/Content/home/css/font-awesome.min.css",
                "~/Content/home/css/swiper.min.css",
                "~/Content/home/css/theme-5.css",
                "~/Content/home/css/site.css"
            ));

#if !DEBUG
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}
