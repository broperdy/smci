﻿using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using SMCi.DataAccess;
using SMCi.DataAccess.AspNetIdentity;
using SMCi.DataAccess.Entities;
using SMCi.Resources;
using SMCi.Rest.Jira;
using SMCi.Rest.Zendesk;
using SMCi.WebCore.AspNetIdentity;
using SMCi.WebCore.Extensions;
using SMCi.WebCore.Services;
using SMCi.WebCore.Tasks;
using SMCi.WebUI.Infrastructure.SiteMap.SimpleInjector;
using Core.DataAccess.Context;
using Core.DataAccess.Uow;
using Core.Dependency.Extensions;
using Core.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using MvcSiteMapProvider.Loader;
using MvcSiteMapProvider.Web.Mvc;
using MvcSiteMapProvider.Xml;
using Owin;
using SimpleInjector;
using SimpleInjector.Advanced;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using SimpleInjector.Lifestyles;

namespace SMCi.WebUI
{
    public class DependencyConfig
    {
        public static void Register(IAppBuilder app)
        {
            var container = new Container();
            var scopeLifestyle = new AsyncScopedLifestyle();

            var hybrid = Lifestyle.CreateHybrid(
                  () => scopeLifestyle.GetCurrentScope(container) != null,
                  scopeLifestyle,
                  new WebRequestLifestyle());

            container.Options.DefaultScopedLifestyle = hybrid;
            container.Options.AllowOverridingRegistrations = true;
            //container.Options.AutoWirePropertiesWithAttribute<InjectAttribute>();

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.RegisterMvcIntegratedFilterProvider();

            container.RegisterDefaultDepdendencies(ResourceManagerHelper.GetAll());
            container.RegisterDefaultConventions("SMCi.WebCore");

            container.RegisterCollection<IRunAtInit>(new[] { typeof(SystemInitializer).Assembly });

            container.Register(() =>
              container.IsVerifying()
                  ? new OwinContext(new Dictionary<string, object>()).Authentication
                  : HttpContext.Current.GetOwinContext().Authentication, Lifestyle.Scoped);

            container.Register<SignInManager>(Lifestyle.Scoped);
            container.Register<IUserStore<User, int>, UserStore>(Lifestyle.Scoped);
            container.Register<IRoleStore<Role, int>, RoleStore>(Lifestyle.Scoped);
            container.Register<RoleManager>(Lifestyle.Scoped);
            container.Register<UserManager>(Lifestyle.Scoped);
            container.RegisterInitializer<UserManager>(manager => manager.Configure(app));
          
            container.Register<IDataContext>(() => new EfContext(), Lifestyle.Scoped);
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);

            container.Register<ICurrentUser, CurrentUser>(Lifestyle.Scoped);
            container.Register<IJiraRestContext, JiraRestContext>(Lifestyle.Scoped);
            container.Register<IZendeskRestContext, ZendeskRestContext>(Lifestyle.Scoped);

            MvcSiteMapProviderContainerInitializer.SetUp(container);
            container.RegisterCacheManager();
            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            InitializeMvcSiteMap(container);
        }

        private static void InitializeMvcSiteMap(Container container)
        {
            // Setup global sitemap loader (required)
            MvcSiteMapProvider.SiteMaps.Loader = container.GetInstance<ISiteMapLoader>();
            // Check all configured .sitemap files to ensure they follow the XSD for MvcSiteMapProvider (optional)
            var validator = container.GetInstance<ISiteMapXmlValidator>();
            validator.ValidateXml(HostingEnvironment.MapPath("~/Mvc.sitemap"));
            // Register the Sitemaps routes for search engines (optional)
            XmlSiteMapController.RegisterRoutes(RouteTable.Routes);
        }
    }
}