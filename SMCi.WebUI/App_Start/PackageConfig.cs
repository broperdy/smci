﻿using System;
using System.IO;
using Core.AutoMapper;
using Serilog;

namespace SMCi.WebUI
{
    public class PackageConfig
    {
        public static void Register()
        {
            Log.Logger = new LoggerConfiguration()
              .MinimumLevel.Debug()
              .WriteTo.RollingFile(
                pathFormat: Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Logs\log-{Date}.txt"),
                fileSizeLimitBytes: 1024 * 1024 * 10, // 10MB
                outputTemplate: "{Timestamp:HH:mm:ss} [{Level}] [{SourceContext}] {Message}{NewLine}{Exception}")
            .CreateLogger();

            AutoMapperConfigurator.Configure("SMCi.WebCore");
#if DEBUG
            //HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkProfiler.Initialize();
#endif
        }
    }
}