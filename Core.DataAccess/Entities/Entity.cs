using System;
using Core.DataAccess.Infrastructure;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DataAccess.Entities
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }

    public abstract class EntityBase : IObjectState
    {
        [NotMapped]
        public ObjectState ObjectState { get; set; }

        public void MarkAsDeleted()
        {
            ObjectState = ObjectState.Deleted;
        }
    }

    public abstract class EntityVersionable : EntityBase
    {
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }

    public abstract class Entity<T> : EntityBase, IEntity<T>
    {
        public virtual T Id { get; set; }
    }

    public abstract class CreationTimeEntity<T> : Entity<T>, IHasCreationTime
    {
        public DateTime CreatedDate { get; set; }
    }

    public abstract class AuditedTimeEntity<T> : CreationTimeEntity<T>, IHasAuditedTime
    {
        public DateTime UpdatedDate { get; set; }
    }

    public abstract class EntityVersionable<T> : EntityVersionable, IEntity<T>
    {
        public virtual T Id { get; set; }
    }
}
