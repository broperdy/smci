﻿namespace Core.DataAccess.Entities
{
    public interface IMustHaveTenant
    {
        int TenantId { get; set; }
    }
}
