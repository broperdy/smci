using Core.DataAccess.Context;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Core.DataAccess.Identity
{
    public abstract class AspNetUserStore<TUser, TRole> : UserStore<TUser, TRole, int, AspNetUserLogin, AspNetUserRole, AspNetUserClaim>
        where TUser : AspNetUser
        where TRole : AspNetRole
    {
        protected AspNetUserStore(IDataContext context)
            : base((DbContext)context)
        {
        }
    }
}