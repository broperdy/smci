using Core.DataAccess.Entities;
using Core.DataAccess.Infrastructure;
using Core.DataAccess.Uow;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Core.DataAccess.Repositories
{
    /// <summary>
    /// Object graph repository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Repository<T> : ReadOnlyRepository<T>, IRepository<T> where T : class, IObjectState
    {
        public Repository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public void Insert(T entity)
        {
            entity.ObjectState = ObjectState.Added;
            DbSet.Add(entity);
        }

        public void Insert(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Insert(entity);
            }
        }

        public void Update(T entity)
        {
            AttachIfNot(entity);
            MarkAsModified(entity);
        }

        public void Update(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Update(entity);
            }
        }

        public void Delete(T entity)
        {
            AttachIfNot(entity);
            if (entity is ISoftDelete)
            {
                ((ISoftDelete)entity).IsDeleted = true;
                MarkAsModified(entity);
            }
            else
            {
                entity.ObjectState = ObjectState.Deleted;
                DbSet.Remove(entity);
            }
        }

        public void Delete(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Delete(entity);
            }
        }

        public void DeleteByUniqueKey(Func<T, bool> predicate)
        {
            var entity = DbSet.Local.SingleOrDefault(predicate) ?? DbSet.SingleOrDefault(predicate);
            if (entity != null)
            {
                Delete(entity);
            }
        }

        protected virtual void AttachIfNot(T entity)
        {
            if (!DbSet.Local.Contains(entity))
            {
                DbSet.Attach(entity);
            }
        }

        private void MarkAsModified(T entity)
        {
            entity.ObjectState = ObjectState.Modified;
            var objectContextAdapter = DbContext as IObjectContextAdapter;
            objectContextAdapter?.ObjectContext.ObjectStateManager
                .ChangeObjectState(entity, EntityState.Modified);
        }
    }
}