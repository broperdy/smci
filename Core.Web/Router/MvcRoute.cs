﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.Extensions;

namespace Core.Web.Router
{
    public class MvcRoute
    {
        private List<MvcRoute> _parentNodes;
        private List<MvcRoute> _visibleChildNodes;

        public MvcRouter Router { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Area { get; set; }
        public string Icon { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Linkable { get; set; }
        public bool Visible { get; set; }
        public List<MvcRoute> ChildNodes { get; set; }
        public MvcRoute ParentNode { get; set; }

        public bool CanOverrideArea => ParentNode != null && Area == null;
        public bool HasChildNodes => ChildNodes != null && ChildNodes.Any();
        public bool IsRoot => ParentNode == null;

        public List<MvcRoute> VisibleChildNodes
        {
            get { return _visibleChildNodes ?? (_visibleChildNodes = ChildNodes.Where(x => x.Visible).ToList()); }
        }

        public bool HasVisibleChildNodes => VisibleChildNodes.Any();

        public MvcRoute()
        {
            ChildNodes = new List<MvcRoute>();
        }

        internal void AddChild(MvcRoute child)
        {
            child.ParentNode = this;
            ChildNodes.Add(child);
        }

        public IEnumerable<MvcRoute> GetParentNodes()
        {
            if (_parentNodes != null)
            {
                return _parentNodes;
            }

            var stack = new Stack<MvcRoute>();
            var current = ParentNode;
            while (current != null)
            {
                stack.Push(current);
                current = current.ParentNode;
            }

            _parentNodes = stack.ToList();
            return _parentNodes;
        }

        public void OverrideArea()
        {
            if (CanOverrideArea)
            {
                Area = ParentNode.Area;
            }
        }

        internal static MvcRoute UntitledNode
        {
            get
            {
                var node = new MvcRoute
                {
                    Title = "Untitled",
                    Router = MvcRouter.Current
                };

                if (HttpContext.Current != null)
                {
                    var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;
                    node.Controller = routeValues["controller"].ChangeTypeTo<string>(string.Empty);
                    node.Action = routeValues["action"].ChangeTypeTo<string>(string.Empty);
                    node.Area = routeValues["area"].ChangeTypeTo<string>(string.Empty);
                }

                return node;
            }
        }
    }
}