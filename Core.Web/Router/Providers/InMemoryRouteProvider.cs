using CuttingEdge.Conditions;

namespace Core.Web.Router.Providers
{
    public class InMemoryRouteProvider : AbstractMvcRouteProvider
    {
        private readonly MvcRoute _rootNode;

        public InMemoryRouteProvider(MvcRoute rootNode)
        {
            Condition.Requires(rootNode).IsNotNull();
            _rootNode = rootNode;
        }

        public override MvcRouter Load()
        {
            return BuildRouter(_rootNode);
        }
    }
}