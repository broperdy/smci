﻿namespace Core.Web.Mvc.Alerts
{
    public enum AlertType
    {
        Success,
        Warning,
        Info,
        Error
    }
}