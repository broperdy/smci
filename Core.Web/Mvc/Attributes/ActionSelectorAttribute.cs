﻿using System;
using System.Reflection;
using System.Web.Mvc;

namespace Core.Web.Mvc.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ActionSelectorAttribute : ActionNameSelectorAttribute
    {
        public string Name { get; }
        public string Sender { get; set; } = "submitter";

        public ActionSelectorAttribute(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException(name);
            }
            Name = name;
        }

        public override bool IsValidName(ControllerContext controllerContext, string actionName, MethodInfo methodInfo)
        {
            if (!actionName.Equals(Name, StringComparison.OrdinalIgnoreCase)) return false;

            var value = controllerContext.Controller.ValueProvider.GetValue(Sender);
            return value != null && methodInfo.Name.Equals(value.AttemptedValue, StringComparison.OrdinalIgnoreCase);
        }
    }
}
