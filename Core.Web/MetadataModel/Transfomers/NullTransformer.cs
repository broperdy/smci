using Core.Web.MetadataModel.Common;
using System;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Transfomers
{
    internal class NullTransformer : IAttributeTransformer
    {
        public static Lazy<NullTransformer> Instance = new Lazy<NullTransformer>(() => new NullTransformer());

        private NullTransformer()
        {

        }

        public void Transform(ModelMetadata metadata, Attribute attribute, HtmlAttributeDictionary dict)
        {
            //NOP        
        }
    }
}