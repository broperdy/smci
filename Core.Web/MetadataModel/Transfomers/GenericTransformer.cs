﻿using Core.Localization;
using Core.Web.MetadataModel.Common;
using CuttingEdge.Conditions;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Transfomers
{
    public abstract class GenericTransformer<T> : IAttributeTransformer where T : Attribute
    {
        protected ILocalizationConfig Localization => DependencyResolver.Current.GetService<ILocalizationConfig>();

        public void Transform(ModelMetadata metadata, Attribute attribute, HtmlAttributeDictionary dict)
        {
            var attr = attribute as T;
            if (attr == null) return;

            SafeTransform(metadata, attr, dict);
        }

        protected abstract void SafeTransform(ModelMetadata metadata, T attribute, HtmlAttributeDictionary dict);

        protected string ErrorMessage<TAttribute>(ModelMetadata metadata, TAttribute attribute)
            where TAttribute : ValidationAttribute
        {
            return Localization.IsEnabled
                    ? Localization.Source.ErrorMessage(metadata, attribute, metadata.DisplayName)
                    : attribute.ErrorMessage;
        }

        protected string FormatErrorMessage<TAttribute>(ModelMetadata metadata, TAttribute attribute, params object[] parameters)
            where TAttribute : ValidationAttribute
        {
            Condition.Requires(parameters).IsLongerOrEqual(1);

            return Localization.IsEnabled
                    ? Localization.Source.FormatErrorMessage(metadata, attribute, parameters)
                    : attribute.FormatErrorMessage((string)parameters[0]);
        }
    }
}