﻿using Core.Localization.Sources;
using Core.Web.MetadataModel.Common;
using System.Web.Mvc;
using Humanizer;

namespace Core.Web.MetadataModel.Transfomers
{
    public class CompareTransformer : GenericTransformer<System.ComponentModel.DataAnnotations.CompareAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, System.ComponentModel.DataAnnotations.CompareAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Data("compare", $"#{attribute.OtherProperty}");
            dict.AddRuleMessage("compare", FormatErrorMessage(metadata, attribute,
                metadata.DisplayName,
                Localization.Source.GetModelString(metadata.ContainerType, attribute.OtherProperty)));
        }
    }
}