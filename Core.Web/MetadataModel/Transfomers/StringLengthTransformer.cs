﻿using Core.Web.MetadataModel.Common;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Transfomers
{
    public class StringLengthTransformer : GenericTransformer<StringLengthAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, StringLengthAttribute attribute, HtmlAttributeDictionary dict)
        {
            if (attribute.MinimumLength > 0)
            {
                dict.AddRuleMessage("minlength", FormatErrorMessage(metadata, attribute, metadata.DisplayName, attribute.MinimumLength, attribute.MaximumLength));
                dict.Data("minlength", attribute.MinimumLength);
            }
       
            dict.Attr("maxlength", attribute.MaximumLength);
            //dict.Data("val-length-max", attribute.MaximumLength);
        }
    }
}
