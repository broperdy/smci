﻿using Core.Localization;
using Core.Localization.Sources;
using Core.Web.MetadataModel.Filters;
using Humanizer;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Core.Web.MetadataModel
{
    /// <summary>
    /// Custom DataAnnotationsModelMetadataProvider to use custom filters
    /// </summary>
    public class ExtensibleModelMetadataProvider : DataAnnotationsModelMetadataProvider
    {
        private readonly IModelMetadataFilter[] _metadataFilters;
        private readonly ILocalizationConfig _localization;

        public ExtensibleModelMetadataProvider(IModelMetadataFilter[] metadataFilters, ILocalizationConfig localization)
        {
            _metadataFilters = metadataFilters;
            _localization = localization;
        }

        protected override ModelMetadata CreateMetadata(
         IEnumerable<Attribute> attributes,
         Type containerType,
         Func<object> modelAccessor,
         Type modelType,
         string propertyName)
        {
            var attributeList = new List<Attribute>(attributes);
            var metadata = base.CreateMetadata(attributeList, containerType, modelAccessor, modelType, propertyName);

            if (containerType == null || propertyName == null) return metadata;

            TranslateMetadata(metadata, containerType, propertyName);

            TransformMetadata(metadata, attributeList);

            return metadata;
        }

        private void TransformMetadata(ModelMetadata metadata, IReadOnlyCollection<Attribute> attributeList)
        {
            foreach (var modelMetadataFilter in _metadataFilters)
            {
                modelMetadataFilter.TransformMetadata(metadata, attributeList);
            }
        }

        private void TranslateMetadata(ModelMetadata metadata, Type containerType, string propertyName)
        {
            if (metadata.DisplayName == null)
                metadata.DisplayName = Translate(containerType, propertyName);

            if (metadata.Watermark == null)
                metadata.Watermark = TranslateOrDefault(containerType, propertyName, "Watermark", metadata.DisplayName);

            // TODO Uncomment if need in the future
            //if (metadata.Description == null)
            //    metadata.Description = Translate(containerType, propertyName, "Description");

            //if (metadata.NullDisplayText == null)
            //    metadata.NullDisplayText = Translate(containerType, propertyName, "NullDisplayText");

            //if (metadata.ShortDisplayName == null)
            //    metadata.ShortDisplayName = Translate(containerType, propertyName, "ShortDisplayName");
        }

        protected virtual string Translate(Type containerType, string propertyName, string metadataName = null)
        {
            return _localization.IsEnabled
                ? _localization.Source.GetModelString(containerType, propertyName, metadataName)
                : propertyName.Titleize();
        }

        protected virtual string TranslateOrDefault(Type containerType, string propertyName, string metadataName, string defaultText)
        {
            if (_localization.IsEnabled)
            {
                var text = _localization.Source.GetModelStringOrNull(containerType, propertyName, metadataName);
                if (string.IsNullOrWhiteSpace(text))
                {
                    text = defaultText;
                }
                return text;
            }
            return defaultText;
        }
    }
}