﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Filters
{
    public interface IModelMetadataFilter
    {
        void TransformMetadata(ModelMetadata metadata, IReadOnlyCollection<Attribute> attributes);
    }
}