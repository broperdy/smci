using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Validations
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FileUploadAttribute : ValidationAttribute, IMetadataAware
    {
        public const int DefaultMaxSizeInMB = 10;
        public static readonly string[] DefaultFileExtensions = {
            ".txt", ".pdf", ".doc", ".docx", ".xls", ".xlsx", ".SMCi", ".html",
            ".zip", ".rar",
            ".mp3", ".mp4", ".avi", ".flv",
            ".gif", ".jpg", ".jpeg",".png"  };

        public int MaxSizeInMB { get; set; } = DefaultMaxSizeInMB;
        public bool IsRequired { get; set; } = true;
        public string[] AllowedExtensions { get; set; }

        public FileUploadAttribute()
        {
            ErrorMessage = "{0} is invalid";
        }

        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            if (file == null && !IsRequired)
            {
                return true;
            }

            return IsFileTypeValid(file) && 
                   file?.ContentLength <= MaxSizeInMB * 1024 * 1024;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            if (string.IsNullOrWhiteSpace(metadata.DataTypeName))
            {
                metadata.DataTypeName = DataType.Upload.ToString();
            }
        }

        private bool IsFileTypeValid(HttpPostedFileBase file)
        {
            var extension = Path.GetExtension(file.FileName);
            return !string.IsNullOrWhiteSpace(extension) &&
                   (AllowedExtensions ?? DefaultFileExtensions).Contains(extension, StringComparer.OrdinalIgnoreCase);
        }
    }
}