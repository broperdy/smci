﻿using System.ComponentModel.DataAnnotations;

namespace Core.Web.MetadataModel.Validations
{
    public class BooleanRequiredAttribute : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            return (bool)value;
        }
    }
}
