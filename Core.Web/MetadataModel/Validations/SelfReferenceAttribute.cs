﻿using System.ComponentModel.DataAnnotations;
using Core.Extensions;

namespace Core.Web.MetadataModel.Validations
{
    public class SelfReferenceAttribute : ValidationAttribute
    {
        public string OtherProperty { get; }

        public SelfReferenceAttribute(string otherProperty)
        {
            OtherProperty = otherProperty;
            ErrorMessage = "{0} does not allow self-reference";
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            var otherPropValue = context.ObjectInstance.GetType().GetProperty(OtherProperty).GetValue(context.ObjectInstance, null);
            var otherValue = otherPropValue.ChangeTypeTo<int?>();
            if (otherValue.HasValue && otherValue.Value == value.ChangeTypeTo<int?>())
            {
                return new ValidationResult(FormatErrorMessage(context.DisplayName));
            }
            return ValidationResult.Success;
        }
    }
}