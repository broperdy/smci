﻿using System;

namespace Core.Web.MetadataModel.Attributes
{
    public class UIOptionsAttribute : Attribute
    {
        public bool Deferred { get; set; } = true;
        public bool ShowLabel { get; set; } = true;
        public string HelpText { get; set; }
        public string OptionLabel { get; set; }
        public bool Disabled { get; set; }
    }
}
