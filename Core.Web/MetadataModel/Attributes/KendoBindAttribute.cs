﻿using Core.Web.MetadataModel.Common;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Attributes
{
    public class KendoBindAttribute : AdditionalHtmlAttribute
    {
        private readonly string _source;
        private readonly string _attr;
        public string Bindings { get; set; }
   
        public KendoBindAttribute(string attr = "value", string source = null)
        {
            _source = source;
            _attr = attr;
        }

        public override void AddAttributes(ModelMetadata metadata, HtmlAttributeDictionary dict)
        {
            if (!string.IsNullOrWhiteSpace(Bindings))
            {
                dict.Data("bind", Bindings);
                return;
            }

            dict.Data("bind", string.Format("{0}: {1}", _attr, _source ?? metadata.PropertyName));
        }
    }
}