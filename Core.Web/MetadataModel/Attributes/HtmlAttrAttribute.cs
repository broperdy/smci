﻿using Core.Web.MetadataModel.Common;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Core.Extensions;
using Core.Infrastructure.Objects;

namespace Core.Web.MetadataModel.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class HtmlAttrAttribute : AdditionalHtmlAttribute
    {
        private readonly string _keyValueAttributes;
        private readonly string _key;
        private readonly object _value;

        private IEnumerable<KeyValue> KeyValues
        {
            get
            {
                var list = new List<KeyValue>();
                if (!string.IsNullOrWhiteSpace(_keyValueAttributes))
                {
                    list.AddRange(_keyValueAttributes.ToKeyValues());
                }
                else
                {
                    list.Add(new KeyValue
                    {
                        Key = _key,
                        Value = _value.ToString()
                    });
                }
                return list;
            }
        }

        public HtmlAttrAttribute(string key, object value)
        {
            _key = key;
            _value = value;
        }

        public HtmlAttrAttribute(string keyValueAttributes)
        {
            _keyValueAttributes = keyValueAttributes;
        }

        public override void AddAttributes(ModelMetadata metadata, HtmlAttributeDictionary dict)
        {
            foreach (var item in KeyValues)
            {
                dict.Attr(item.Key, item.Value);
            }
        }
    }
}