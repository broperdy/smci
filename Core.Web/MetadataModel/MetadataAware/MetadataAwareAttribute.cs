﻿using System;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.MetadataAware
{
    public abstract class MetadataAwareAttribute : Attribute, IMetadataAware
    {
        public abstract void OnMetadataCreated(ModelMetadata metadata);
    }
}