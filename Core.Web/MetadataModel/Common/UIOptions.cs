﻿using Core.Web.MetadataModel.Attributes;

namespace Core.Web.MetadataModel.Common
{
    public class UIOptions
    {
        public bool Deferred => Common.Deferred;
        public bool ShowLabel => Common.ShowLabel;
        public string HelpText => Common.HelpText;
        public string OptionLabel => Common.OptionLabel;
        public bool Disabled => Common.Disabled;

        public DateOptionsAttribute Date { get; set; }
        internal UIOptionsAttribute Common { get; set; }
    }
}
