using System;
using Core.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;

namespace Core.Web.MetadataModel.Common
{
    public class HtmlAttributeDictionary : Dictionary<string, object>
    {
        public HtmlAttributeDictionary()
            : base(StringComparer.OrdinalIgnoreCase)
        {

        }

        public void AddRuleMessage(string rule, string message)
        {
            Attr($"data-{rule}-msg", message);
        }

        public HtmlAttributeDictionary Placeholder(string placeholder)
        {
            if (!string.IsNullOrWhiteSpace(placeholder))
            {
                Attr("placeholder", placeholder);
            }
            return this;
        }

        public HtmlAttributeDictionary Attr(string key, object value)
        {
            this.Merge(key, value);
            return this;
        }

        public HtmlAttributeDictionary Attr(IDictionary<string, object> attributes)
        {
            Merge(attributes);
            return this;
        }

        public HtmlAttributeDictionary Data(string key, object value)
        {
            this.Merge($"data-{key}", value);
            return this;
        }

        public HtmlAttributeDictionary AddClass(object value)
        {
            AppendInValue("class", " ", value);
            return this;
        }

        public HtmlAttributeDictionary AddStyle(string value)
        {
            AppendInValue("style", ";", value);
            return this;
        }

        public HtmlAttributeDictionary AttrIfNotNull(string key, object value)
        {
            if (value != null)
            {
                Attr(key, value);
            }
            return this;
        }

        public string ToKvpAttributes()
        {
            return string.Join(";", this.Select(kvp => $"{kvp.Key}:{kvp.Value}"));
        }


        public IDictionary<string, object> AppendInValue(string key, string separator,
            object value)
        {
            this[key] = ContainsKey(key)
                ? this[key] + separator + value
                : value.ToString();
            return this;
        }

        public IDictionary<string, object> PrependInValue(string key, string separator,
            object value)
        {
            this[key] = ContainsKey(key)
                ? value + separator + this[key]
                : value.ToString();
            return this;
        }

        public string ToAttributeString(IDictionary<string, object> instance)
        {
            var stringBuilder = new StringBuilder();
            foreach (var keyValuePair in instance)
            {
                stringBuilder.Append(string.Format(" {0}=\"{1}\"",
                    HttpUtility.HtmlAttributeEncode(keyValuePair.Key),
                    HttpUtility.HtmlAttributeEncode(keyValuePair.Value.ToString())));
            }
            return stringBuilder.ToString();
        }

        public IDictionary<string, object> Merge(IDictionary<string, object> from,
            bool replaceExisting = true)
        {
            if (from == null)
            {
                return this;
            }

            var appendKeys = new[] { "class" };

            foreach (var keyValuePair in from)
            {
                if (replaceExisting || !ContainsKey(keyValuePair.Key))
                {
                    if (appendKeys.Contains(keyValuePair.Key))
                    {
                        AppendInValue(keyValuePair.Key, " ", keyValuePair.Value);
                    }
                    else
                    {
                        this[keyValuePair.Key] = keyValuePair.Value;
                    }
                }
            }
            return this;
        }

        public IDictionary<string, object> Merge(object values, bool replaceExisting = true)
        {
            return Merge(new RouteValueDictionary(values), replaceExisting);
        }
    }
}