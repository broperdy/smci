﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Core.Extensions;
using Core.Web.FluentUI.Builders;
using Core.Web.MetadataModel.Common;

namespace Core.Web.FluentUI.Helpers
{
    public class FluentHelper<TModel>
    {
        protected readonly HtmlHelper<TModel> Helper;

        public FluentHelper(HtmlHelper<TModel> helper)
        {
            Helper = helper;
        }

        public GenericTagBuilder ParagraphFor(string name)
        {
            return new GenericTagBuilder("p", name);
        }

        public GenericTagBuilder ParagraphFor<TProp>(Expression<Func<TModel, TProp>> expression)
        {
            var text = ModelMetadata.FromLambdaExpression(expression, Helper.ViewData).Model?.ToString();
            return ParagraphFor(Helper.NameFor(expression).ToString()).Text(text);
        }

        public CheckBoxBuilder CheckBox(string name)
        {
            return new CheckBoxBuilder().Name(name);
        }

        public CheckBoxBuilder CheckBoxFor<TProp>(Expression<Func<TModel, TProp>> expression)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, Helper.ViewData);

            return CheckBox(Helper.NameFor(expression).ToString())
                .Checked(metaData.Model.ChangeTypeTo<bool>(false))
                .DisplayName(metaData.DisplayName)
                .ShowLabel(metaData.Options().ShowLabel);
        }
    }
}