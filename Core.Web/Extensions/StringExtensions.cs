﻿using System;
using System.IO;
using System.Web;
using System.Web.Hosting;

namespace Core.Web.Extensions
{
    public static class StringExtensions
    {
        public static string ToUniqueFileName(this HttpPostedFileBase file)
        {
            return Guid.NewGuid().ToString("N") + Path.GetExtension(file.FileName);
        }

        public static string ToAbsolutePath(this string path)
        {
            return HostingEnvironment.MapPath(path);
        }
    }
}
