﻿using System.Net.Mime;
using System.Web;

namespace Core.Web.Extensions
{
    public static class HttpExtensions
    {
        public static void AddContentDispositionHeader(this HttpResponseBase response, string fileName, bool inline = false)
        {
            var cd = new ContentDisposition
            {
                FileName = fileName,
                Inline = inline
            };

            response.Headers.Add("Content-Disposition", cd.ToString());
        }
    }
}
