﻿using System.Web.SessionState;

namespace Core.Web.Caching
{
    /// <summary>
    /// User-level caching (user session)
    /// </summary>
    public interface IPerRequestCache : ISessionCache
    {

    }
}
