﻿using System;

namespace Core.Web.Caching
{
    public class NullSessionCache : ISessionCache
    {
        public void Set(string key, object value)
        {
        }

        public bool Contains(string key)
        {
            return false;
        }

        public void Clear()
        {
        }

        public object Get(string key)
        {
            return null;
        }

        public T Get<T>(string key)
        {
            return default(T);
        }

        public T GetOrAdd<T>(string key, T value)
        {
            return value;
        }

        public T GetOrAdd<T>(string key, Func<T> funcGetValue)
        {
            return funcGetValue();
        }

        public void Remove(string key)
        {
        }
    }
}