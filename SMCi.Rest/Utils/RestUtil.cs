﻿using RestSharp;

namespace SMCi.Rest.Utils
{
    public static class RestUtil
    {
        public static T Get<T>(RestClient client)
        {
            var activator = ActivatorHelper.Instance.GetActivator<T>();
            return activator(client);
        }
    }
}
