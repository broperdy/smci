﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Core.Extensions;

namespace SMCi.Rest.Utils
{
    /// <summary>
    /// Making reflection fly and exploring delegates
    /// https://vagifabilov.wordpress.com/2010/04/02/dont-use-activator-createinstance-or-constructorinfo-invoke-use-compiled-lambda-expressions/
    /// http://rogeralsing.com/2008/02/28/linq-expressions-creating-objects/
    /// </summary>
    internal sealed class ActivatorHelper
    {
        private static readonly Lazy<ActivatorHelper> LazyInstance
            = new Lazy<ActivatorHelper>(() => new ActivatorHelper());

        private ActivatorHelper()
        {
        }

        public static ActivatorHelper Instance => LazyInstance.Value;

        private readonly Dictionary<Type, dynamic> _dict = new Dictionary<Type, dynamic>();

        public ObjectActivator<T> GetActivator<T>()
        {
            var key = typeof(T);
            try
            {
                if (!_dict.ContainsKey(key))
                {
                    _dict.Add(key, DelegateExtensions.GetCompileConstructor<T>());
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine( exception);
            }
          
            return (ObjectActivator<T>)_dict[key];
        }
    }
}
