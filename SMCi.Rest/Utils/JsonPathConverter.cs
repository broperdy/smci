﻿using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Caching;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp.Extensions;

namespace SMCi.Rest.Utils
{
    public class JsonPathConverter : JsonConverter
    {
        public override bool CanWrite => false;

        public override object ReadJson(JsonReader reader, Type objectType,
            object existingValue, JsonSerializer serializer)
        {
            var jo = JObject.Load(reader);
            var targetObj = Activator.CreateInstance(objectType);

            var properties = GetProperties(objectType);

            foreach (var prop in properties.Where(p => p.CanRead && p.CanWrite))
            {
                var att = prop.GetCustomAttributes(true)
                    .OfType<JsonPropertyAttribute>()
                    .FirstOrDefault();

                var jsonPath = att != null ? att.PropertyName : prop.Name.ToCamelCase(CultureInfo.InvariantCulture);
                var token = jo.SelectToken(jsonPath);

                if ((token != null) && (token.Type != JTokenType.Null))
                {
                    var value = token.ToObject(prop.PropertyType, serializer);
                    prop.SetValue(targetObj, value, null);
                }
            }

            return targetObj;
        }


        public override bool CanConvert(Type objectType)
        {
            // CanConvert is not called when [JsonConverter] attribute is used
            return false;
        }

        public override void WriteJson(JsonWriter writer, object value,
            JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        private static PropertyInfo[] GetProperties(Type objectType)
        {
            if (!MemoryCache.Default.Contains(objectType.FullName))
            {
                MemoryCache.Default.Add(
                     objectType.FullName,
                     objectType.GetProperties(),
                     new CacheItemPolicy
                     {
                         SlidingExpiration = TimeSpan.FromMinutes(10)
                     });
            }
            return MemoryCache.Default.Get(objectType.FullName) as PropertyInfo[];
        }
    }
}