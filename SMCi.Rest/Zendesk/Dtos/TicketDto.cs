﻿namespace SMCi.Rest.Zendesk.Dtos
{
    public class TicketDto
    {
        public string Subject { get; set; }
        public RequesterDto Requester { get; set; }
        public CommentDto Comment { get; set; }
        public string Priority { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
    }
}
