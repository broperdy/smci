﻿namespace SMCi.Rest.Zendesk.Dtos
{
    public class NewTicketDto
    {
        public TicketDto Ticket { get; set; }
    }
}
