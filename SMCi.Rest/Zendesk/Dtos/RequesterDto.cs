namespace SMCi.Rest.Zendesk.Dtos
{
    public class RequesterDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}