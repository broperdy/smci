﻿using SMCi.Rest.Infrastructure;
using SMCi.Rest.Zendesk.Api;
using RestSharp.Authenticators;

namespace SMCi.Rest.Zendesk
{
    public interface IZendeskRestContext : IRestContext
    {
        RequestRest Requests { get; }
    }

    public class ZendeskRestContext : AbstractRestContext, IZendeskRestContext
    {
        private const string ZendeskApiUrl = "https://coresoftwarestudio.zendesk.com/";


        public ZendeskRestContext()
            : base(ZendeskApiUrl)
        {
            Authenticator = new HttpBasicAuthenticator("SMCi@gmail.com", "12345678x@X");
        }

        public RequestRest Requests => Rest<RequestRest>();
    }
}
