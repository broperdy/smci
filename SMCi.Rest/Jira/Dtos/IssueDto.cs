﻿using System;
using SMCi.Rest.Utils;
using Newtonsoft.Json;

namespace SMCi.Rest.Jira.Dtos
{
    [JsonConverter(typeof(JsonPathConverter))]
    public class IssueDto
    {
        public string Key { get; set; }

        [JsonProperty("fields.created")]
        public DateTime Created { get; set; }

        [JsonProperty("fields.summary")]
        public string Summary { get; set; }

        [JsonProperty("fields.status.name")]
        public string Status { get; set; }
    }
}
