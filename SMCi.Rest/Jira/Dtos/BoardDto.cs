﻿namespace SMCi.Rest.Jira.Dtos
{
    public class BoardDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
