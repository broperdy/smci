﻿using System;

namespace SMCi.Rest.Jira.Dtos
{
    public class SprintDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int OriginBoardId { get; set; }
    }
}