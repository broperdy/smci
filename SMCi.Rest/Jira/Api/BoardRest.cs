﻿using System.Collections.Generic;
using System.Linq;
using SMCi.Rest.Extensions;
using SMCi.Rest.Infrastructure;
using SMCi.Rest.Jira.Dtos;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace SMCi.Rest.Jira.Api
{
    public class BoardRest : RestApiBase
    {
        public BoardRest(RestClient restClient)
            : base(restClient, "board")
        {
        }

        public IList<BoardDto> GetBoards()
        {
            return JObject.Parse(ExecuteGet().Content)["values"]
                          .ToObjectList<BoardDto>();
        }

        public SprintDto GetActiveSprint(int boardId)
        {
            return JObject.Parse(ExecuteGet($"{boardId}/sprint?state=active").Content)["values"]
                          .ToObjectList<SprintDto>()
                          .FirstOrDefault();
        }

        public IList<IssueDto> GetIssues(int boardId, int sprintId)
        {
            return JObject.Parse(ExecuteGet($"{boardId}/sprint/{sprintId}/issue").Content)["issues"]
                          .ToObjectList<IssueDto>();
        }
    }
}
