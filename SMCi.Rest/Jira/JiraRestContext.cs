﻿using SMCi.Rest.Infrastructure;
using SMCi.Rest.Jira.Api;
using RestSharp.Authenticators;

namespace SMCi.Rest.Jira
{
    public interface IJiraRestContext : IRestContext
    {
        BoardRest Boards { get; }
    }

    public class JiraRestContext : AbstractRestContext, IJiraRestContext
    {
        private const string JiraApiUrl = "https://SMCi.atlassian.net/rest/agile/1.0/";

        public JiraRestContext()
            : base(JiraApiUrl)
        {
            Authenticator = new HttpBasicAuthenticator("SMCi@gmail.com", "ThinhDuyx@X");
        }

        public BoardRest Boards => Rest<BoardRest>();
    }
}
