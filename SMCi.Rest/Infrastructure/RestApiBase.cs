﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RestSharp;
using RestSharp.Newtonsoft.Json;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;
using RestRequest = RestSharp.RestRequest;

namespace SMCi.Rest.Infrastructure
{
    public abstract class RestApiBase
    {
        protected readonly RestClient RestClient;
        protected readonly string BaseResource;

        protected RestApiBase(RestClient restClient, string baseResource)
        {
            RestClient = restClient;
            BaseResource = baseResource;
        }

        protected IRestResponse ExecuteGet()
        {
            var request = new RestRequest(BaseResource, Method.GET);
            return RestClient.Execute(request);
        }

        protected IRestResponse ExecuteGet(string resource)
        {
            var request = new RestRequest($"{BaseResource}/{resource}", Method.GET);
            return RestClient.Execute(request);
        }

        protected IRestResponse ExecutePost(string resource, object content)
        {
            var request = new RestRequest($"{BaseResource}/{resource}", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer(JsonSerializer.Create(new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }));
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.RequestFormat = DataFormat.Json;
            request.AddBody(content);

            return RestClient.Execute(request);
        }
    }
}