﻿using System;
using System.Collections.Generic;
using SMCi.Rest.Utils;
using RestSharp;

namespace SMCi.Rest.Infrastructure
{
    public abstract class AbstractRestContext : RestClient
    {
        private readonly object _lock = new object();
        private Dictionary<Type, dynamic> _cache = new Dictionary<Type, dynamic>();
        private Dictionary<Type, dynamic> Cache => _cache ?? (_cache = new Dictionary<Type, dynamic>());

        protected AbstractRestContext(string baseUrl)
        {
            BaseUrl = new Uri(baseUrl);
        }

        public T Rest<T>() where T : class
        {
            var type = typeof(T);
            if (!Cache.ContainsKey(type))
            {
                lock (_lock)
                {
                    if (!Cache.ContainsKey(type))
                    {
                        Cache.Add(type, RestUtil.Get<T>(this));
                    }
                }
            }
            return Cache[type];
        }
    }
}