
create table public."VanBanPhapQuy"(
"Id" BIGSERIAL PRIMARY KEY,
"TieuDe" varchar (255) NULL,
"NoiDung" varchar NULL,
"GhiChu" varchar NULL,
"DocUrl" varchar NULL,
"iStatus" int  NULL DEFAULT (0) ,
"DisplayOrder" int  NULL DEFAULT (0) );

create table public."ThongTinTiepNhan"(
"Id" BIGSERIAL PRIMARY KEY,
"HoTen" varchar NULL,
"Email" varchar NULL,
"TieuDe" varchar (255) NULL,
"NoiDung" varchar NULL,
"TraLoi" varchar NULL,
"iStatus" int  NULL DEFAULT (0) );
