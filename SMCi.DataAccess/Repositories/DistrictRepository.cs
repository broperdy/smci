﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using SMCi.DataAccess.Entities;

namespace SMCi.DataAccess.Repositories
{
    public class DistrictRepository : Repository<District>
    {
        public DistrictRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
