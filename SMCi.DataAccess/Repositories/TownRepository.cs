﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using SMCi.DataAccess.Entities;

namespace SMCi.DataAccess.Repositories
{
    public class TownRepository : Repository<Town>
    {
        public TownRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
