﻿using SMCi.DataAccess.Entities;
using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;

namespace SMCi.DataAccess.Repositories
{
    public class RoleRepository : Repository<Role>
    {
        public RoleRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}