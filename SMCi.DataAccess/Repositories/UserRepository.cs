﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using SMCi.DataAccess.Entities;
using SMCi.DataAccess.Enums;
using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;

namespace SMCi.DataAccess.Repositories
{
    public class UserRepository : Repository<User>
    {
        public UserRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Task<int> GetHostId()
        {
            return AsNoFilter().Where(x => x.UserName == ERole.Host.ToString().ToLower()).Select(x => x.Id).SingleOrDefaultAsync();
        }
    }
}