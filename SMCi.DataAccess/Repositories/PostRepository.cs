﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using SMCi.DataAccess.Entities;

namespace SMCi.DataAccess.Repositories
{
    public class PostRepository : Repository<Post>
    {
        public PostRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
