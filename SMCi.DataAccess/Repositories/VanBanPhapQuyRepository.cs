﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using SMCi.DataAccess.Entities;

namespace SMCi.DataAccess.Repositories
{
    public class VanBanPhapQuyRepository : Repository<VanBanPhapQuy>
    {
        public VanBanPhapQuyRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
