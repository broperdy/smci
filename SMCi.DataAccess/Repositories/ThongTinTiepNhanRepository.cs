﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using SMCi.DataAccess.Entities;

namespace SMCi.DataAccess.Repositories
{
    public class ThongTinTiepNhanRepository : Repository<ThongTinTiepNhan>
    {
        public ThongTinTiepNhanRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
