﻿namespace SMCi.DataAccess.Enums
{
    public enum EnumiStatus
    {
        Pending,
        Accepted,
        Rejected
    }
}
