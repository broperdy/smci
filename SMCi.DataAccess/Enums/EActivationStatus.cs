﻿

namespace SMCi.DataAccess.Enums
{
    public enum EActivationStatus
    {
        Pending,
        Accept,
        Cancelled,
        Registered
    }
}
