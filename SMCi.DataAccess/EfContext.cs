﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using SMCi.DataAccess.Entities;
using Core.DataAccess.Context;
using Core.DataAccess.Entities;
using Core.DataAccess.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Z.EntityFramework.Plus;

namespace SMCi.DataAccess
{
    public class EfContext : DataContext<User, Role>
    {
        static EfContext()
        {
            Database.SetInitializer<EfContext>(null);
        }

        public IDbSet<Post> Posts { get; set; }
        public IDbSet<VanBanPhapQuy> VanBanPhapQuys { get; set; }
        public IDbSet<ThongTinTiepNhan> ThongTinTiepNhansl { get; set; }
        public IDbSet<District> Districts { get; set; }
        public IDbSet<Town> Towns { get; set; }
        public IDbSet<Company> Companies { get; set; }

        public EfContext()
            : this("DefaultConnection")
        {
        }

        public EfContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            this.Filter<ISoftDelete>(q => q.Where(x => !x.IsDeleted));
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Entity<User>().ToTable("SMCi_User");
            modelBuilder.Entity<Role>().ToTable("SMCi_Role");
            modelBuilder.Entity<Post>().ToTable("SMCi_Post");
            modelBuilder.Entity<VanBanPhapQuy>().ToTable("SMCi_VanBanPhapQuy");
            modelBuilder.Entity<ThongTinTiepNhan>().ToTable("SMCi_ThongTinTiepNhan");
            modelBuilder.Entity<AspNetUserRole>().ToTable("SMCi_UserRole");
            modelBuilder.Entity<District>().ToTable("MstDistrict");
            modelBuilder.Entity<Town>().ToTable("MstTown");
            modelBuilder.Entity<Company>().ToTable("MstCompanyNew");
        }
    }
}