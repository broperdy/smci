using SMCi.DataAccess.Entities;
using Core.DataAccess.Context;
using Core.DataAccess.Identity;

namespace SMCi.DataAccess.AspNetIdentity
{
    public class UserStore : AspNetUserStore<User, Role>
    {
        public UserStore(IDataContext context)
            : base(context)
        {
        }
    }
}