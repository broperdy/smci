﻿using System.Threading.Tasks;
using SMCi.DataAccess.Entities;
using SMCi.DataAccess.Enums;
using Microsoft.AspNet.Identity;

namespace SMCi.DataAccess.AspNetIdentity.Extensions
{
    public static class UserManagerExtensions
    {
        public static async Task<IdentityResult> AddToRoleAsync(this UserManager userManager, User user, ERole role)
        {
            return await userManager.AddToRoleAsync(user.Id, role.ToString());
        }
    }
}
