﻿using SMCi.DataAccess.Entities;
using Microsoft.AspNet.Identity;

namespace SMCi.DataAccess.AspNetIdentity
{
    public class UserManager : UserManager<User, int>
    {
        public UserManager(IUserStore<User, int> store)
            : base(store)
        {
        }
    }
}