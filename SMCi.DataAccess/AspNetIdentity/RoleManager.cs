﻿using SMCi.DataAccess.Entities;
using Microsoft.AspNet.Identity;

namespace SMCi.DataAccess.AspNetIdentity
{
    public class RoleManager : RoleManager<Role, int>
    {
        public RoleManager(IRoleStore<Role, int> store)
            : base(store)
        {
        }
    }
}