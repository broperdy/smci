using SMCi.DataAccess.Entities;
using Core.DataAccess.Context;
using Core.DataAccess.Identity;

namespace SMCi.DataAccess.AspNetIdentity
{
    public class RoleStore : AspNetRoleStore<Role>
    {
        public RoleStore(IDataContext context)
            : base(context)
        {
        }
    }
}