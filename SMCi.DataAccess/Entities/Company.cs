﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.DataAccess.Entities;

namespace SMCi.DataAccess.Entities
{
    public class Company : EntityBase
    {
        [Key]
        public int CompanyID { get; set; }
        public int CompanyType { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName1 { get; set; }
        public string CompanyAreaBusiness { get; set; }
        public string CompanyName2 { get; set; }
        public string CompanyAddr { get; set; }
        public int ProvinceID { get; set; }
        public int DistrictID { get; set; }
        public int TownID { get; set; }
        public string CompanyTel { get; set; }
        public string CompanyFax { get; set; }
        public string CompanyMail { get; set; }
        public string CompanyWebsite { get; set; }
        public string TaxCode { get; set; }
        public string RegisterNumber { get; set; }
        public string RegisterDate { get; set; }
        public string RegisterPlace { get; set; }
        public string BankNumber { get; set; }
        public string BankName { get; set; }
        public int RepresentedID { get; set; }
        public string CompanyLogo { get; set; }
        public string CompanyHP { get; set; }
        public int IsParent { get; set; }
        public int CompanyStatus { get; set; }
        public decimal CompanyX { get; set; }
        public decimal CompanyY { get; set; }

        [ForeignKey("DistrictID")]
        public District District { get; set; }
    }
}
