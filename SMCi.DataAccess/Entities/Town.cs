﻿using System.ComponentModel.DataAnnotations;
using Core.DataAccess.Entities;

namespace SMCi.DataAccess.Entities
{
    public class Town : EntityBase
    {
        [Key]
        public int TownID { get; set; }
        public int DistrictID { get; set; }
        public string TownCode { get; set; }
        public string TownName { get; set; }
        public int TownStatus { get; set; }
        public int DisplayOrder { get; set; }
    }
}
