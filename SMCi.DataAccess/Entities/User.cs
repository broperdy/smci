﻿using System.Security.Claims;
using System.Threading.Tasks;
using SMCi.DataAccess.AspNetIdentity;
using Core.DataAccess.Identity;
using Core.Timing;
using Microsoft.AspNet.Identity;

namespace SMCi.DataAccess.Entities
{
    public class User : AspNetUser
    {
        public bool IsLocked => LockoutEnabled && LockoutEndDateUtc.HasValue && LockoutEndDateUtc.Value.Date > Clock.Now.Date;

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Avatar { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager manager, string authenticationType = DefaultAuthenticationTypes.ApplicationCookie)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here

            return userIdentity;
        }

        public void SetLockState(bool isLocked)
        {
            if (isLocked)
            {
                LockoutEnabled = true;
                LockoutEndDateUtc = Clock.Now.AddYears(200);
            }
            else
            {
                LockoutEndDateUtc = null;
            }
        }
    }
}