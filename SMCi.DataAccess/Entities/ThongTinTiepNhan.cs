﻿using System;
using Core.DataAccess.Entities;
using SMCi.DataAccess.Enums;

namespace SMCi.DataAccess.Entities
{
    public class ThongTinTiepNhan : Entity<int>, IHasCreationTime
    {
        public string HoTen { get; set; }
        public string Email { get; set; }
        public string TieuDe { get; set; }
        public string NoiDung { get; set; }
        public string TraLoi { get; set; }
        public EnumiStatus iStatus { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
