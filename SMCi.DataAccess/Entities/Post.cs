﻿using Core.DataAccess.Entities;

namespace SMCi.DataAccess.Entities
{
    public class Post : Entity<int>
    {
        public string Image { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
