﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.DataAccess.Entities;

namespace SMCi.DataAccess.Entities
{
    public class District : EntityBase
    {
        [Key]
        public int DistrictID { get; set; }
        public int ProvinceID { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public int DistrictStatus { get; set; }
        public int DisplayOrder { get; set; }
    }
}
