﻿using System;
using Core.DataAccess.Entities;
using SMCi.DataAccess.Enums;

namespace SMCi.DataAccess.Entities
{
    public class VanBanPhapQuy : Entity<int>, IHasCreationTime
    {
        public string TieuDe { get; set; }
        public string NoiDung { get; set; }
        public string DocUrl { get; set; }
        public string GhiChu { get; set; }
        public EnumiStatus iStatus { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsPublish { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
